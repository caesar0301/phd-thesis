% # -*- coding: utf-8-unix -*-
\chapter{绪论}\label{chap:intro}

随着移动技术的飞速发展，人类社会积累了大量的通信数据。这些数据借助于移动设备搭载
的微型传感器，有的直接记录了用户的移动位置、网络服务的使用，有的间接记录了周边的
环境信息、以及用户的社交关系。和采用传统调研方式采集的数据集相比，移动网络数据具
有样本规模大、数据更新度高、采集代价小的特点。在本文中，我们简称这类数据为``移动
大数据''。移动大数据以其自身的规模优势，减少了传统分析方法对先验分布假设的依赖，
使得行为科学领域（Behavior Science）以前难以直接研究的问题变得可行，从而推动了近
年来对大规模人类行为规律、及其产生机理的研究。

人类的时空行为\footnote{如无特殊说明，本文中时空行为特指人类的时空行为，二者在表
  述上具有相同的含义。}记录了个体或群体在特定空间、时间范围内的移动性。作为近年来
行为科学领域的热点问题之一，人类时空行为研究，不仅有助于移动网络运营商优化网络性
能和服务体验\supercite{chaintreau_impact_2007}，而且在流行疾病传播控
制\supercite{dalziel_human_2013, wesolowski_commentary:_2014}、城市交通系统规
划\supercite{congosto_microbloggers_2015}、以及群体性事件的检测和预
防\supercite{woodcock_health_2014}等有着重要的应用价值。另一方面，利用移动大数据
对时空行为进行分析和建模，不仅对正在发展中的大数据研究理论具有参考价值，而且新的
分析方法和算法模型，对于类时空数据挖掘问题（即通过转换可等价为时空行为分析的领域
问题）具有较强的普适作用。

本章作为全文的基础，主要阐述了人类时空行为研究的背景。在给出时空行为定义的基础上，
讨论了时空行为挖掘的应用和理论价值，以及国内外近年来的研究趋势。通过对有影响的国
际会议、期刊上的相关工作进行深度调研，分别从时空规律、算法模型、以及方法论角度进
行了归纳总结。在此基础上，梳理了人类时空行为挖掘中仍待解决的关键问题，以及本文的
主要研究工作及创新点。最后给出了全文的结构安排。

\section{人类时空行为的研究背景}

自本世纪初智能手机设备普及以来，人类的通信方式不断向便携式和多样化发展。种类丰富
的移动应用，不仅满足了人们在通信、休闲、消费、社交等诸多方面的需求，也推动了移动
互联网的用户数量呈指数增长。据中国互联网信息中心（CNNIC）第36次《中国互联网发展状
况统计报告》，截止2015年6月，我国的互联网普及率为48.8\%，其中手机用户规模达到
了5.49亿，占互联网用户总人数的88.9\%。丰富的移动应用，促使人们的日常通信不再局限
于传统的语音通话，而是逐渐被社交网络、即时消息等新型通信方式所取代。在这两方面因
素的共同作用下，移动网络流量呈现爆炸式增长。据爱立信最新的移动市场报
告\footnote{\url{http://www.ericsson.com/mobility-report}}，2015年全球移动网络流
量（图\ref{fig:ericssonforecast:a}）约为4090PB/月，较2014年增加了77.8\%，以亚太地
区（APAC）为代表的世界各区域将以47\%$\sim$65\%的速率逐年（至2020年）递增。移动用
户群体和网络流量的激增，潜在地促进了无线通信技术的升级换代。传统GSM/GPRS网络的低
速率移动通信技术，正逐步被传输效率更高的4G以及LTE/5G技术所取代。如
图\ref{fig:ericssonforecast:b}所示，2015年GSM网络注册用户36.08亿，是LTE/5G用户
的367\%；到2020年，这一比例将缩小到35\%，从而更好地满足人们日益增长的网络资源和服
务体验需求。这些国内外不同机构的统计数据表明，我们正在步入一个真正的``移动大数
据''时代。

\begin{figure}[!tb]
  \centering
  \subfigure[移动网络流量]{
    \label{fig:ericssonforecast:a}
    \includegraphics[width=0.45\textwidth]{chap1/traffic-forecast.jpeg}}
  \hspace{0.05\textwidth}
  \subfigure[移动网络注册用户量]{
    \label{fig:ericssonforecast:b}
    \includegraphics[width=0.45\textwidth]{chap1/subscription-forecast.jpeg}}
  \bicaption[fig:ericssonforecast]{全球移动网络流量和注册用户量趋势预测}{全球移动
    网络流量和注册用户量趋势预测，数据来自爱立信，2015}{Fig}{Prediction of
    mobile network traffic and subscription globally, Ericssion, 2015.}
\end{figure}

% \begin{figure}[!tb]
%  \centering
%  \includegraphics[width=0.7\textwidth]{chap1/subscription-forecast.jpeg}
%  \bicaption[fig:subscriptionforecast]{移动网络注册用户数量的按年预测值} {移动
%  网络注册用户数量的按年预测值（数据来自爱立信，2015）} {Fig}{Subscription
%  forecast over different technologies, data from Erission, 2015.}
% \end{figure}

\subsection{时空行为的定义}

移动网络应用在提供服务的同时，还在以多种不同的形式数字化着人类的生活。现代的智能
设备不但硬件体积小、携带方便，而且搭载了功能丰富的微型传感器，例如GPS和网络模块能
够辅助进行空间定位，加速度传感器可以感知用户的运动状态。基于硬件传感器开发的移动
服务，为用户提供便捷生活的同时，也在人类历史上首次实现了大规模的、低成本的数据采
集\supercite{congosto_microbloggers_2015}。例如风靡全球的基于位置定位的游
戏Ingress\footnote{\url{https://www.ingress.com/}} 由Google公司开发，成功实现了人
们以娱乐的方式共享真实时空数据。另外一些服务（如Foursqure）利用基于地理位置的签到
数据，为用户提供更加个性化的服务和推荐内容，这些内容反过来帮助服务提供商理解特定
位置对于用户的重要性。在网络空间(Cyber Space) 中，如果将不同的网络服务（以域名为
标识）看作独立的``空间位置''，超文本链接为连接不同位置的道路，那么用户在网络空间
的使用偏好和浏览行为，则被海量的移动网络数据记录着。除此以外，现实生活中人类时空
行为，还被其他网络系统或服务记录着\supercite{asgari_survey_2013}，如城市公共交通
的电子刷卡系统、自行车租赁服务、以及出租车GPS系统等，以起止（OD）点的形式在较粗空
间粒度上记录了乘客的通勤行为；货币流通网络\supercite{brockmann_money_2008}从货币
交换的过程感知人类长距离的旅行。由于移动网络通常覆盖的空间范围广、用户群体大、采
集成本低，本文以多尺度下的移动网络数据为基础，通过给出时空行为的一般形式，使提出
的分析方法及理论模型，可以在相似数据集或时空问题上同样适用。

\begin{defn}[个体时空行为] \label{def:stbehavior}个体时空行为指在给定时间、空间范
  围内，观测对象表现出来的一系列空间位置转换的序列，表示为$\mathcal{B} := \{
  (\mathbf{s}_i, t_i, \mathbf{c}_i) | i = 0,1,2,\ldots \}$，式中$\mathcal{B}$表示
  观测到的\emph{时空行为序列}，其中每个元素$\mathbf{b} = (\mathbf{s}, t,
  \mathbf{c}) \in \mathcal{B}$，称作\emph{时空行为向量}，且$\mathbf{s} \in
  \mathcal{D}^{|\mathbf{s}|}$表示$|\mathbf{s}|$维的空间位置向量，$t \in
  \mathcal{T}$表示时间标量，$\mathbf{c} \in
  \mathcal{X}^{|\mathbf{c}|}$表示$|\mathbf{c}|$维的场景信息向量。
\end{defn}

在实际研究中，针对不同来源、不同质量的时空数据集，定义\ref{def:stbehavior}中的时
空行为向量和序列会有不同的形式。通过将不同背景下的时空行为用统一的形式进行表征，
不但可以利用相似的算法和模型对人类时空行为进行研究，而且从不同角度揭示了人类时空
行为的普遍规律和内在联系。本文着重讨论两种不同的时空行为，即物理空间中的\emph{移
  动行为}和网络空间中的\emph{用户参与行为}。对于移动行为，空间向量$\mathbf{s}$记
录了用户的物理位置，如经纬度，从而可以通过欧氏距离、球面距离、或者路网距离对用户
的移动行为进行量化分析；场景向量$\mathbf{c}$包括空间和时间的相关属性，如地点热
度、POI特征、日期等。对于用户参与行为，如果将网络服务映射为网络空间中的位置，则空
间向量$\mathbf{s}$表示用户在时刻$t$所处的网络空间位置，超链接关系和服务使用次序构
成了空间中的转移行为；相应的场景向量$\mathbf{c}$表示用户参与网络服务时的环境信息，
如物理位置、网络状况等。

\subsection{时空行为的研究价值}

对人类的时空行为规律进行挖掘的重要价值，不仅体现在对已有的社会活动现象（如大规模
群体事件）的形成、消失过程进行定量分析，充分了解其发展脉络；而且能够结合历史事件
规律，和已有部分观测信息，对未来同类事件发生的可能性、规模、以及趋势等提前把握，
帮助管理员和决策者做出合理而准确的规划。本节从应用和理论两个角度，结合实际应用场
景和案例，对时空行为的研究价值进行了阐述。

\subsubsection{应用价值}

传播型疾病通常以个体接触的形式进行扩散，因此和人类的时空行为紧密相
关\supercite{dalziel_human_2013, eubank_modelling_2004, kraemer_big_2015,
  tatem_use_2009, wesolowski_commentary:_2014}。例如作为人类历史上爆发的最严重的
传染疾病之一，埃博拉（Ebola）病
毒\footnote{\url{https://en.wikipedia.org/wiki/Ebolavirus}}截止2015年9月已造
成11,306人失去了生命。该病毒主要通过接触被感染人或动物的体液进行传播，从初次爆发
的非洲中部，一直蔓延到西非（如几内亚）和非洲北部（如苏丹），并在美国、西班牙等地
区发现疑似和确诊病例。这种``跨洋越海''的病毒传播方式，正是借助于人类的移动行为实
现的。Wesolowski等\supercite{wesolowski_commentary:_2014}基于移动运营商的手机通话
记录（Call Detail Record, CDR），对埃博拉疫区以及周围若干国家的人口分布、行为模式
等进行了分析。研究结果表明，对于埃博拉病毒的肆虐，除了和当地政府的卫生条件和政策
制定有关以外，地区内、地区间的人口流动是另一决定性因素。因此，对人口流动强度的分
析，有助于对下一次爆发的时间和地点实现前瞻性把握，及时做好预防措施。例如，研究者
发现，西非地区的人口比目前较爆发集中的中非地区流动性更强，有着潜在的大规模爆发风
险。

随着现代城市化进程的加快，城市所承担的功能不再是满足人们的日常所需，如商品和工作
机会，更重要的是实现生活便捷的同时，提供高质量的生活环境和城市智慧。高效率的城市
交通、城市污染监控、群体性应急事件预防等一直是``智慧城
市''领域\supercite{daquin_smart_2015}关注的热点问题。利用城市系统产生的各种时空数
据，如公交刷卡、移动网络数据等，对城市居民的出行方式和行为从大尺度上进行把握，不
仅可以优化城市交通资源\supercite{congosto_microbloggers_2015}，降低居民出行成本，
而且结合居民的时空行为规律，能够实现智慧化的生活服务，如针对下班后要去菜场购物的
上班族推出的``一站式''配送服务。另一方面，对城市居民的时空行为进行长期分析，能够
对区域土地的使用、人口通勤分布提供量化指标\supercite{woodcock_health_2014}，为城
市管理者的未来建设规划提供有力的依
据。Woodcock等\supercite{woodcock_health_2014}利用伦敦市自行车共享系统的740万租赁
记录，从空气污染和交通事故伤害角度，量化分析了自行车共享系统对居民健康程度的影响；
结果从数据角度肯定了自行车共享系统对减小居民死亡风险的好处，同时发现男人较女人、
老人较年轻人风险降低程度更高。

从移动网络自身来看，研究网络中的用户移动模式和上网行为，对优化网络资源和开发新型
网络协议都大有裨益。对网络服务提供商（ISP）而言，用户的网络体验是网络优化的首要目
标。但是从用户端到服务端，中间涉及的硬件和软件组件众多，一旦用户反馈问题很难从单
个用户记录里诊断出故障所在。通过对网络中大量用户的时空行为数据进行采集，分析在相
似约束条件下（如空间位置、时间段、网络服务类型、终端硬件、软件等）批量用户的网络
体验数据，便能以较高的可信度诊断出用户故障反馈的真实性、以及故障原因所在。对网络
组件和协议开发者而言，用户的时空行为研究有助于从大尺度（如城市、国家）上了解网络
负载的时空分布特性，开发出符合用户使用规律的动态资源调度的网络器件；同时，结合用
户移动行为和网络参与行为的移动网络流量模型，因为减小了对流量先验分布特征假设的依
赖，在开发新型网络协议（如机会网络协议\supercite{chaintreau_impact_2007}）时，较
纯统计的流量模型\supercite{paul_understanding_2011}更准确、更灵活。


\subsubsection{理论价值}

作为行为科学的一个子领域，时空行为研究从时间和空间维度对人类的行为进行探索，并从
理论上对时空行为规律进行总
结。2005年Barab\'{a}si\supercite{barabasi_origin_2005}对人类行为在时间上的非泊松
特性进行了研究，修正了以往理论研究中对人类行为符合泊松分布的假设。在空间上，人们
发现了莱维飞行（L\'evy Flight）特性\supercite{brockmann_scaling_2006}，并且在停留
地点数\supercite{song_modelling_2010}、地点偏
好\supercite{gonzalez_understanding_2008}、停留时
间\supercite{song_modelling_2010}、接触时间\supercite{mei_swim:_2009}等分布中观测
到了重尾分布的特点。通过对一段时间段内人群在空间上的分布数据进行分析，人们在传统
重力模型\supercite{schneider_gravity_1959}、介入机会模
型\supercite{stouffer_intervening_1940}的基础上提出了辐射模
型\supercite{simini_universal_2012}。这些统计规律和理论模型的一般性，为行为科学自
身的不断发展、以及与其他学科的交叉融合上提供了基础。

时空行为分析的另一个贡献是对网络科学理论的发展。人类的社会活动随着时间的积累，会
演化出不同空间下的网络结构，如社交行为演化出了社交网络、日常的通勤行为构成了通勤
网络（即描述了城市里不同地点之间的人群流动方向和数量）。这些特定场景下的网络结构，
为探索复杂网络的新特性的发现提供了基础。例如，通过对大规模社交网络中用户的交友关
系进行研究\supercite{toole_coupling_2015, wang_human_2011}，人们发现了网络科学里
称之为``小世界''（Small World）\supercite{mei_swim:_2009}的新特性，即单个节点到达
其他节点的平均距离一般较小；对应到现实生活中，两个陌生人之间以较高的概率通过数目
不多的熟人连接在一起。另一方面，人类的行为随着时间在变化着，形成的网络结构也在相
应地发生着改变，因此衍生出对动态复杂网络理论的研
究\supercite{aggarwal_evolutionary_2014,
  holme_temporal_2012}。Holme等\supercite{holme_temporal_2012}基于包括个人通信行
为在内的多种网络结构，总结了时序网络（Temporal Network）分析的度量指标和理论模
型。

此外，人类时空行为与经济学的交叉研究，也为经济理论的发展提供了新的视
角\supercite{eagle_network_2010, arcaute_constructing_2015, toole_tracking_2015,
  bettencourt_urban_2010}。在区域经济关系的研究
中，Eagle等\supercite{eagle_network_2010}利用国家尺度的手机网络通信数据，研究了网
络多样性和空间经济结构之间的关系；分析结果发现区域通信模式的多样性（包括社交网络
和空间网络）是区域经济健康程度的一个``指示器''。在城市经济的研究
中，Arcaute等\supercite{arcaute_constructing_2015}分析了国家尺度上城市的空间分布
和各项经济指标之间的关系，并提出了用功能性的``Urban''定义代替传统上以行政单位为基
础的``City''划分。综上所述，时空行为研究中的理论方法和模型，对行为科学、网络科学
以及经济学的研究和发展起到了补充和推进作用。

\subsection{时空行为的研究趋势}

自从人类时空行为的非泊松特性被首次发现\supercite{barabasi_origin_2005}以来，国内
外涌现出一批长期致力于时空行为研究的组织。在国际上，美国西北大学物理系
的Barab\'asi课题组\footnote{\url{http://barabasi.com/}}从物理学角度对人类时空行为
进行理解，研究特色是基于统计力学理论，将个体看作物理粒子、人群看作受特定约束的粒
子系统，进而从不同尺度上对粒子的行为进行研究。以计算科学为背景的麻省理工学院
（MIT）多媒体实验室人类行为课题组\footnote{\url{http://hd.media.mit.edu/}}，结合
计算机和大数据的处理优势，对人类在交通系统、健康、能源、以及金融系统里的行为进行
研究。此外，MIT环境工程系Gonz\'alez领导的HumNet课题
组\footnote{\url{http://humnetlab.mit.edu/}}，从复杂网络角度对人类的时空行为进行
研究，在移动模式挖掘问题上有着长期的积累。

同一时期国内也涌现出一批优秀的研究组织。北京城市实验室（Beijing City
Lab\footnote{\url{http://www.beijingcitylab.com/}}）通过虚拟组织的形式将一批研究
人员聚集在一起，研究特色是结合多种类型的数据，对城市系统里的重要环节（如区域规划、
城市经济等）进行量化的分析。微软亚洲研究院的城市计算
组\footnote{\url{http://research.microsoft.com/en-us/projects/urbancomputing}}致
力于利用异构的城市数据和机器学习的理论，对城市生活中的实际问题（如交通资源优化、
空气污染、房产价格等）进行了模型统计和实证分析。电子科技大学的周涛所在课题组，是
国内人类行为动力学研究的代表，同时利用物理模型和复杂网络理论对时空行为规律进行研
究。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.7\textwidth]{chap1/paper-trend.pdf}
  \bicaption[fig:paper-trend]{2005至2015年间发表的有影响力国际会议和期刊论文}
  {2005至2015年间发表的有影响力国际会议和期刊论文} {Fig}{Influential papers on
    human mobility that are published during 2005 and 2015.}
\end{figure}

图\ref{fig:paper-trend}展示了2005至2015十年间发表在有影响力的国际会议和期刊的论文
数量。图中数据检索自计算机科学专业论文数据库DBLP，按照中国计算机协会推荐的会议和
期刊进行过滤。从中可以看出，人类时空行为的有影响力的研究成果数量，在最近五年得到
了80\%$\sim$93\%的提升。这类现象的产生，一方面受人类行为新成果的驱动，另一方面受
益于近年来后智能化移动设备的普及，为大规模采集匿名行为数据提供了可能。

\section{人类时空行为挖掘的国内外研究进展}
\label{sec:review}

作为一个交叉领域，时空行为研究者来自计算机科学、通信工程、物理学、地理信息学等多
个领域，从各自领域的视角揭示着人类行为的内在规律。本节对2005$\sim$2015年间有代表
性的研究工作进行了调研，并从数据集、研究方法、行为规律、理论模型等角度进行了对比
分析。

\subsection{人类移动行为研究进展}

\subsubsection{时空统计特征}
\label{sec:review:ststat}

\emph{时间特征}: 人类长期以来遵循着``日出而作，日落而息''的生活规律。随着社会的发
展，这样的作息规律和生活习惯、地理分布、职业种类等结合起来，衍生出复杂的时空移动
行为。尽管如此，时间特征依然为我们揭示了人类生活中的一些普遍规
律。Barab\'asi\supercite{barabasi_origin_2005}从动力学角度出发，对人类行为在时间
上满足泊松分布的假设提出了质疑，并在通信数据中发现了非泊松分布以及阵发的行为特征。
为了探究产生这一新特性的机制，作者分析了三种队列服务模型：先进先出、随机服务、以
及基于优先权的服务，并利用基于优先权的队列服务模型，对人类日常行为中表现出来的幂
律（Power-Law）现象进行了解释。Goh等\supercite{goh_burstiness_2008}提出了阵发参
数$\Delta$和记忆参数$\mu$用来衡量人类行为的间隔时间分布，并通过阵发-记忆象限图，
对现有的时间行为模型是否能完全捕捉人类行为的阵发与记忆特征进行了研
究。Barab\'asi模型和Goh参数均是对\emph{事件间隔时间}的刻画。地点的\emph{停留时间}通
常也满足幂律的分布特征\supercite{song_modelling_2010, brockmann_scaling_2006}。连
续时间的随机游走模型\supercite{ brockmann_scaling_2006}（CTRW）捕捉了单个地点的停
留时间分布：针对每一个由随机游走模型产生的新地点，对应的停留时间由给定参数的幂律
分布采样得出。

另一个重要的时间统计特征是\emph{接触时间间隔}（Inter-contact time，ICT），即不同
个体在连续两次接触时的间隔时间。Mei等\supercite{mei_swim:_2009}从实际数据中观测
到ICT通常具有二分性（Dichotomy），即ICT的概率分布函数在低值部分满足幂律分布，在高
值部分满足指数分布。这样的二分性被认为是由于移动个体在特定时间内，其移动范围随着
时间的推移趋向一个特定的边界值，因此高值部分的概率随着ICT增大概率急剧下
降。Karagiannis等\supercite{karagiannis_power_2010}发现ICT二分性的临界特征值位
于0.5天附近（即超过0.5天概率分布将按照指数分布下降）并将这种二分性归因于移动个体
对不同地点的偏好差异以及返回时间的幂律分布上。从本文的研究视角来
看，Mei和Karagiannis对ICT二分性的解释本质上是一致的，均基于移动个体对不同地点的访
问偏好不同，而移动范围的边界性和返回时间的幂律分布是访问偏好产生的不同统计结果。

\emph{空间特征}：社会资源在空间上的分布和人类行为的空间特征有着紧密的联系。在早期
的移动模型中，个体的下一地点被认为是随机选择的，也就是所谓的随机游走模型。后来随
着位置服务和应用的兴起，人们通过真实地理位置数据分析，发现个体的移动位置通常表现
出所谓的``莱维飞行''特性\supercite{bazzani_statistical_2010,
  brockmann_scaling_2006, brockmann_money_2008}：虽然在大多数情况下，人们倾向于短
途旅行，但依然存在一定的概率进行长距离远行。莱维飞行可以被认为是布朗运动的一般形
式，同时属于无标度的分形随机过程\supercite{brockmann_scaling_2006}。莱维飞行是一
种相邻地点的转移距离$\lambda$满足幂律分布的马尔可夫随机模型，这意味着$\lambda$概
率分布的二次矩是发散的，并且任意长转移距离都有可能产生。这样过程被称为``简单莱维
飞行''。

事实上，和现实观测对比分析后发现，简单的莱维飞行模型是不完备
的\supercite{brockmann_scaling_2006, gonzalez_understanding_2008}。简单莱维飞行的
转移距离满足无标度的分布，从而使空间扩散形成一个超扩散过程（Super-diffusion
process）\supercite{brockmann_scaling_2006}。而在实际观测中，个体的移动范围有边界
性，意味着移动行为的空间扩散是不断减弱的。Gonz\'alez等通过对十万移动网络用户的匿
名轨迹分析，发现空间转移距离满足结尾的幂律分布，即在$\lambda$超过阈
值$\lambda_0$后其概率分布满足指数分布。用数学形式可以简洁地表达为，$p(\lambda)
\sim (\lambda+\lambda_0)^{-b} \cdot e^{\lambda/k}$，其中$0 < b \leq
2$，$\lambda_0$在不同应用中取值不同。我们称这样的过程为``截尾莱维飞行''。

CTRW模型是结合了简单莱维飞行和停留时间幂律分布的随机游走模
型\supercite{brockmann_scaling_2006}。 虽然其包含了空间转移分布，但访问地点演化以
及访问频率与实际观测数据不符。针对这些不
足，Song等\supercite{song_modelling_2010}基于CTRW模型并考虑了以下空间分布特
征：i) 个体的独立地点数目$L(t)$满足幂律的增长过程，$L(t) \sim t^s, s<1$。和单纯的
随机游走模型和莱维飞行的随机游走模型\supercite{brockmann_scaling_2006}相
比，Song的模型预测了更加准确的地点分布。ii) 地点的访问频率满足齐夫（Zipf）定
律\supercite{bagrow_mesoscopic_2012}，即人类访问地点的频率不是均匀分布的，而是倾
向于频繁访问一小部分常去的地点。利用数学形式表达，$f_k \sim k^{-\zeta}$，其
中$\zeta = 1.2 \pm 0.1$，$k$为地点按照访问频次逆序排列的整数序号。iii) 空间距离满
足超低扩散过程（Ultraslow diffusion）。在CTRW模型中，观测时间越长，个体的访问地点
越远离初始点。而实际观测显示，个体的空间移动范围随着时间推移，将趋近一个特定的空
间边界，即均方转移距离MSD\supercite{song_modelling_2010}满足对数增长分布。

回转半径$R_g$\supercite{gonzalez_understanding_2008}是对个体空间移动能力的有效度
量，其定义为$R_g^2 = \frac{1}{n}\sum_{k=1}^{n}(\mathbf{r}_k -
\mathbf{\bar{r}})^2 $。近年来研究人员从多种角度对$R_g$的特性进行了研
究。Gonz\'alez等\supercite{gonzalez_understanding_2008}首次将回转半径引入到人类时
空行为分析中，并发现个体的$R_g$分布满足截尾的幂律分布，同时群体的转移距离分
布$P(\lambda)$由个体的转移距离分布和群体$R_g$的异质结构共同决定，即$P(\lambda) =
\int_0^{\infty}P(\lambda|R_g)P(R_g)dR_g$。Bagrow等
\supercite{bagrow_mesoscopic_2012}研究了不同活动区域里人们的回转半径分布特征，并
发现在主要活动区域内，$R_g$的分布特征符合对数分布，较全局分布的增长较
缓。Park等\supercite{park_eigenmode_2010}研究了回转半径的时变特征，通过马尔可夫过
程得出了与实际观测一致的时间序列，并将马尔可夫矩阵与时空行为的特征模式分析关联在
了一起。

近年来，人类行为的其他空间特征规律逐渐被揭示了出
来。Calabrese等\supercite{calabrese_interplay_2011}利用手机通信数据对人们的``共现
性''（即手机通话时处在同一基站范围内）进行了研究，分析结果显示超过90\%的用户即使
实际居住地相距较远，但都出现过``共现''行为，为从单纯的通信行为角度量化研究人类的
空间移动提供了支撑。Hossmann等\supercite{hossmann_complex_2011}对移动网络用户的共
现性从复杂网络角度进行了研究。通过将多源数据集的用户空间关系转换成``接触
图''（Contact Graph），分别对接触网络的度分布、小世界特性、以及社区结构等进行了量
化和对比分析。Kang等\supercite{kang_intra-urban_2012}从城市形态学角度（包括城市紧
凑程度和规模）对人类的移动模式进行了研究，结果发现城市内的空间移动距离满足指数分
布，并且对应的指数参数与城市的紧凑程度和规模密切相
关。Frank等\supercite{frank_happiness_2013}基于带有空间位置标签的Twitter数据，将
情感分析和空间移动模式关联起来，分析结果发现人们的快乐程度和与平均位置的距离呈对
数相关。Bora等\supercite{bora_mobility_2014}同样利用社交网络数据，对种族隔离与人
们的移动性之间的关联关系进行了研究，结果发现所有种族都倾向于经常访问同一种族聚集
的区域，非洲裔、亚裔、以及西班牙裔较少出现在白人聚集区域，并且非洲裔聚集的区域被
其他种族访问的可能性最小。Kung等\supercite{kung_exploring_2014}利用手机通话记录、
从不同空间尺度上对城市内的通勤模式进行了研究，数据分析显示通勤时间的分布具有一般
性，和观测的空间尺度相关性较小。

\subsubsection{行为模式挖掘}
\label{sec:review:pm}

时空行为模式挖掘是一类从历史轨迹数据中发现移动模式（Mobility patterns）的方法。行
为模式符合人类认知客观世界的一般规律，即从多个观测对象中发现其共有部分。研究人类
的时空行为模式具有普遍的应用价值和意义。一方面，从杂乱无章的日常轨迹中提取出群体
的出行规律，有助于及时检测出突发性群体事件的发生，以及为事后疏散方案的制定提供有
效支撑。另一方面，从空间尺度上来看，一些人造网络系统，如城市交通网络、电网、移动
网络等，承受着来自人群自由移动带来的潜在系统性风险。模式挖掘有利于将用户的行为规
律，嵌入到网络系统的资源调度中，从而从底层降低系统化风险的产生。本节从可预测性分
析、序列模式挖掘、时空行为结构角度回顾了时空行为模式挖掘的相关研究成果。

\emph{可预测性}\supercite{song_limits_2010}分析首次从信息量角度对时空行为的可预测
性进行了理论研究。可预测性指利用``最合适''的预测算法正确预测用户下一位置的概
率\supercite{song_limits_2010}。研究人员利用三个月的匿名手机通话数据，首先基
于Lempel-Ziv压缩算法对单个用户的轨迹信息熵进行了测定，进而利用Fano不等式推导出可
预测性的理论上限，并将规律性（Regularity）作为其下限。数据分析显示可预测性在熵等
于0.8附近取得峰值，这意味着一个典型用户的下一位置信息量非常小，约为1.74左右。观测
用户的平均可预测性为0.93左右。

\emph{序列模式}是时空行为分析中最基本、也是最常用的轨迹模式之一。按照是否考虑轨迹
序列的时间信息为原则，可以分为无时间序列模式和时间序列模式挖掘。无时间序列模式挖
掘来源于机器学习理论中的一类经典算法，例如Apriori算法
和PrefixSpan算法\supercite{tiakas_searching_2009, jeung_trajectory_2011,
  gong_trajectory_2013}。通过将用户轨迹抽象为一组符号序列，算法输出满足给定支持度
的一组序列模式。该类模式具有算法复杂度低、结果直观等优点，在实际场景中得到了广泛
应用。例如Gong等\supercite{gong_trajectory_2013}利用WiFi网络数据、研究了校园用户
的轨迹模式随时间的变化，以及模式变化和外部公共事件之间的联
系。Tiakas等\supercite{tiakas_searching_2009}在考虑空间网络的前提下，提出了一系列
衡量轨迹序列距离的量化指标，为相似轨迹聚类、用户画像等提供了支撑。

无时间序列模式的挖掘结果拥有两个基本特征：1）模式的子序列也是模式；2）子模式的支
持度不小于父模式的支持度。这决定了该方法在行为分析中的鲁棒性较低：a）由于序列中的
每个位置符号是无权重的，因此位置的模糊和缺失将严重影响长模式的数量；b）序列模式的
本质是一个多阶马尔可夫过程。虽然包含序列信息，但缺少时间和空间上的结构信息。因此
对于序列不同、时空结构相似的用户模式区分能力较弱。

时空序列模式\supercite{cao_mining_2005, giannotti_trajectory_2007,
  tiakas_searching_2009, patel_incorporating_2012, chen_constructing_2014}在保留
空间马尔可夫过程的同时，添加了更多时间信息。其中时间信息可以以多种形式集成到算法
当中，如停留时间、移动间隔时间、到达时间点等。时间信息的类型不同，对应的模式挖掘
算法设计则不同。Patel等\supercite{patel_incorporating_2012}从停留时间角度，基于相
似轨迹段和最小描述长度原则，将多个轨迹数据融合到一个时间加权的轨迹网络中，并实现
了对网络中Top-K轨迹进行分类标注。这个方法的优势是将停留时间作为轨迹段聚类的基础，
从而形成了统一的轨迹网络；不足之处在于，最小描述长度原则下的最优并非都是实际情况
下的最优。Giannotti等\supercite{giannotti_trajectory_2007}和Tiakas等
\supercite{tiakas_searching_2009}从移动间隔时间角度对用户的轨迹序列进行了研究。前
者\supercite{giannotti_trajectory_2007}对无时间序列模式中的序列包含关系进行了一般
性拓展，定义了时间和空间结合的轨迹包含关系，并在此基础上提出了兴趣区域（Region
of Interest, ROI）检测的方法。这个方法的不足在于，轨迹包含关系缺少位置之间的空间
距离信息。后者\supercite{tiakas_searching_2009}首先从单纯的时间角度给出了轨迹的时
间距离，然后通过加权和的方法将空、时距离结合定义了轨迹的时空距离。这个方法的缺陷
在于未考虑时间和空间维度的交互信息。Chen等\supercite{chen_constructing_2014}研究
了包含到达时间点的轨迹数据，建立了用户的轨迹画像，并对无时间和时空结合的轨迹序列
模式进行了对比研究。这个方法的优势是考虑了序列和语义角度的空间距离、并将时空交互
信息融合到了相似度计算当中，不足之处在于缺少单个轨迹内部的时空结构信息。

\emph{时空结构}是近年来逐渐得到重视的概念，指从用户的行为语
义\supercite{zheng_understanding_2008, farrahi_discovering_2011,
  ganti_inferring_2013}和特征模式\footnote{在本文中，特征行
  为\supercite{eagle_eigenbehaviors:_2009}、特征状
  态\supercite{park_eigenmode_2010}、以及特征模式\supercite{qin_patterns_2012}表
  示同一概念，并统称为特征模式。}角度理解人类时空行为规律。一方面，人类的移动行为
表现出不同的语义状态，例如，Zheng等\supercite{zheng_understanding_2008}利用GPS数
据和监督学习算法对行人的交通方式（即步行、私家车、公共汽车）进行研究，并对不同的
特征进行了性能分析。Farrahi等\supercite{farrahi_discovering_2011}借鉴了文本分析当
中的隐含狄利克雷分布（LDA）技术，对带有时间戳的轨迹序列数据进行分析，从中识别出四
种行为状态，并基于这些行为状态对用户的移动规律进行了量化研究。

另一方面，研究者发现人类的移动行为拥有一组特征模
式\supercite{eagle_eigenbehaviors:_2009,park_eigenmode_2010, qin_patterns_2012,
  schneider_unravelling_2013}，这组特征模式各自之间的相关性较小，但是相互之间的组
合构成了多样的行为模式。通过对特征模式的解读，有助于把握人类移动的一些本质规律。
例如Qin等\supercite{qin_patterns_2012}将用户的轨迹数据表示成时空矩阵的形式，然后
应用聚类算法识别出用户的特征模式，并分析了特征模式和轨迹信息熵之间的关
系。Eagle等\supercite{eagle_eigenbehaviors:_2009}基于二值化后的时空矩阵，提出特征
行为（EigenBehavior）的概念，利用主成分分析（PCA）方法对转换后的时空矩阵进行分解，
发现15个特征模式即可以98\%的准确率表达用户的时空行
为。Park等\supercite{park_eigenmode_2010}首先建立了用户轨迹的马尔可夫转移矩阵，然
后对该矩阵进行模分析（Eigenmode analysis），并对特征模式和特征值的物理意义进行了
解释。这类方法的优势是摆脱了序列模式挖掘对马尔可夫性质的依赖，并结合了不同时间段
内的行为信息；不足之处在于分解得到的特征模式数目需要人为确定，且各模式的可解释性
有限。与此不同的是，Schneider等\supercite{schneider_unravelling_2013}通过将用户轨
迹表达成有向无权图的形式，引入了网络科学中``模序''（Motif）的概念，结果发现超
过90\%的用户轨迹可以用17种模序进行表达。但该研究仅考虑了时空行为中的空间结构信息，
以至于缺少时间维度和时空交互信息。

\subsubsection{个体移动行为建模}

行为模型\supercite{karamshuk_human_2011, pirozmand_human_2014,
  gorawski_review_2014, hess_data-driven_2015}是对人类移动的\emph{时空特征}和
\emph{行为模式}的理论抽象和概括。行为模型在客观洞察人类移动规律的基础上，使用带
参数的数学模型、物理过程等方式对行为产生的机理进行模拟，从而达到量化分析和重用的
目的。例如，在新型移动协议开发中，模拟用户在真实场景下的移动行为对协议设计、性能
测量等起着至关重要的作用；对于需要感知用户行为的网络协议
\supercite{karamshuk_human_2011}，将用户行为以模型的形式嵌入到协议当中，协议便可
以根据真实数据对用户行为进行学习，从而利用行为的产生和演化特征对网络协议性能进行
优化。本小节对现有行为模型进行回顾，并根据模型特点分四个类别进行介绍\footnote{更
  多模型可参考Hess调研\supercite{hess_data-driven_2015}的表1，Pirozmand调研的表2，
  以及Karamshuk\supercite{karamshuk_human_2011}和
  Gorawski\supercite{gorawski_review_2014}的调研文献。}：

\emph{随机游走模型}是用户移动性管理中最简单的一种模型，该模型因模拟气体粒子在给
定空间里的随机运动规律而得名。随机游走模型\supercite{gorawski_review_2014}（RW）
描述了一个完全随机的运动过程：单个粒子或个体以随机速度$v \in [V_{min}, V_{max}]$
和随机方向$\theta \in [0, 2\pi]$。随机停靠点模型
\supercite{nguyen_steps-approach_2011}（RWP）在随机游走模型的基础上添加了停留时
间的限制，即粒子在到达下一地点的时候，随机停留时间$t \in [0, T]$。为了捕捉实际观
测中的时空统计特性，莱维随机游走模型\supercite{rhee_levy-walk_2011}（LRW）和连续
时间随机游走模型\supercite{brockmann_scaling_2006}（CTRW）为空间转移距离和停留时
间分别增加了重尾分布的特性。这类模型的不足在于将运动个体看作随机粒子，意味着粒子
的运动过程是无记忆的，也就是下一方向和速度的选择不依赖于以前的状态。

\emph{位置偏好模型}基于实际观测中，人们倾向于访问为数不多的若干位置，并且位置之
间的组合构成了时空行为的主要模式。SLAW模型\supercite{lee_slaw:_2009}基于分形驻留
点模型和最小代价路径规划策略，实现了实际观测中的时空统计特性，如转移距离、停留时
间、ICT的幂律分布特性；结合基于空间簇的个体游走模型，从而捕捉了人们日常生活中对
常去地点的偏好、以及出行路线偏好等。SWIM模型\supercite{mei_swim:_2009}实现了社交
网络中的``小世界''特性，通过模拟现实生活中人们结合距离和受欢迎程度选择目的地的特
点，将社交关系引入到了行为模型中；同时作者从理论上证明了模型中ICT二分性的存在。
TVCM模型\supercite{hsu_modeling_2009}对随机游走模型进行了增强，基于时变的群落
（Community）结构实现了模拟个体的地点偏好特性和周期行为。这类模型往往基于随机游
走模型，通过叠加额外的约束条件（如周期性），使得模型产生更加接近实际观测的统计特
征。这些特征也使得该类模型在实际中得到广泛的应用\supercite{karamshuk_human_2011,
  pirozmand_human_2014}。

\emph{行程规律模型}强调了移动个体的社会属性，移动轨迹的生成是受每天要参与的社会
活动和活动属性所决定的。该类模型的思想来源于实际生活中，人们的行为总是按照一定的
行程规划进行的，如早上八点吃早点，然后在九点前必须到达工作场所；又如晚上五点去学
校接孩子下学。这样的行程规划一般包含三个要素：时间、地点、活动内容。各个要素受一
定的条件约束，且活动内容和属性因人而异，从而能够按照一定的规则产生多样的个体移动
行为。ADMM模型\supercite{zheng_agenda_2010}是该类模型的典型代表，研究人员以模块
的形式对行程要素进行定义，并利用NHTS调研数据生成各要素的具体内容；模型性能也通过
Ad-Hoc网络仿真和网络协议性能分析进行了验证。另一方面，行程规划决定了观测周期内的
时空行为结构，如行为模序\supercite{schneider_unravelling_2013}从空间拓扑角度反映
了行程活动之间的相互关系。PBM扰动模型\supercite{schneider_unravelling_2013}利用
行程规律生成了移动行为的扰动过程，从而较好重现了实际观测中的17种典型行为模序。

\emph{社交关系模型}基于人们的社交活动和位置变化有着潜在的关联。Cho等
\supercite{cho_friendship_2011}利用带有位置标签的社交网络数据研究了这种内在的关
联性，数据分析发现虽然人们在多数情况下在较小的空间范围内活动，但是依然有较高的概
率去远距离的朋友居住地附近；换句话说，\emph{社交关系对远距离移动较近距离移动的影
  响更大}。从量化角度来看，已有社交关系对移动性的影响是移动性对新建社交关系影响
的两倍。尽管如此，研究人员发现单纯依赖社交关系对用户的移动性进行预测，其性能依然
有限。PSMM模型\supercite{cho_friendship_2011}基于时变的一阶马尔可夫过程，将移动
行为的时空周期性特征和社交关系结合在了一起，并从平均签到似然、预测准确率、平均距
离误差角度对模型性能进行了评判。与PSMM中社交关系以条件概率的形式出现不同，HCMM模
型\supercite{boldrini_hcmm:_2010}的主要思想在于用户的位置依赖于拥有社交关系的其
他用户的位置，且用户社交关系越强，影响力越大；同时HCMM模型融合了转移距离满足幂律
分布的空间统计特征。

\subsubsection{群体移动行为建模}

个体移动行为从细粒度上研究人类的时空行为规律，在小尺度、个性化的应用场景中有着重
要的价值。但是对于大尺度的应用，如移动网络资源优化、城市规划等，需要能够对群体移
动行为规律和特点有整体性的把握。以移动网络为例，为了满足移动用户对网络性能和应用
体验的需求，网络运营商通常会在人口密度高的区域部署数目较多的、高带宽的基站，反之
在人口密度稀疏的区域部署较少的、覆盖范围广的基站。同时随着用户在一天内的移动，骨
干网资源（如光传输波长）也会随着用户密度的变化（亦称``潮汐效应''）而进行调整。这
样的场景需要我们从大空间尺度上理解人群的移动规律和时空分布特点。De
Mongis等\supercite{de_montis_structure_2005}研究了意大利撒丁岛各自治区间的人类移
动行为，从复杂网络角度对人群移动网络的群聚系数、边权重、节点强度等基本特征进行了
分析，从较大尺度上对城市的人群交互提供了观测依
据。Jiang等\supercite{jiang_discovering_2012}利用芝加哥的旅行调研数据（TTS）研究
了城市区域之间的群体移动模式；作者基于主成分分析的K-Means聚类，识别出8种人群类型
和5种区域交互模式。Tanahashi等\supercite{tanahashi_inferring_2012}使用匿名
的CDR数据分析了纽约人口的空间分布和时间动态性，并通过朴素贝叶斯模型对移动概率矩
阵和可预测性进行了研究。Deville等\supercite{deville_dynamic_2014}从数据角度论证
了人口普查、遥感信息和匿名手机记录对国家级别、不同时间尺度上人口分布评估的可靠性，
研究结果展示了匿名手机数据对时变应用场景，如区域冲突、疾病扩散等的价值。

对群体移动行为更加深入的理解方式是建立群体行为模型，从量化的角度对群体行为
的\emph{时空分布}进行研究。Schilcher等\supercite{schilcher_measuring_2008}介绍了
一种统一的、客观的指标来衡量给定空间区域内点分布的不均衡（Inhomogeneity）程度，该
指标基于点密度的局部方差来定义，衡量结果和线上主观调研具有较好的一致性。此外，从
网络流量分布角度，Michalopoulou等\supercite{michalopoulou_towards_2011}提出移动流
量的空间分布能够用混合的对数正态分布进行拟合；同时作者预言，在国家尺度范围内，网
络流量的时空分布和群体时空分布紧密相关，而在城市尺度上，出于对城市环境和网络业务
的考虑，二者的相关性并不显著。Lee等\supercite{lee_spatial_2013,
  lee_spatial_2014}提出利用高斯随机场，对具有对数正态分布特点的网络流量空间分布进
行建模。但是，这些成果在研究群体时空分布上依然具有局限性：首先，正如Lee等指出，网
络流量和人群数量的空间分布，既有关联也有差别，二者的模型形式和物理意义仍需进一步
研究；其次，即使将现有的网络流量模型应用在人群时空分布研究上，依然只能反映空间的
静态分布，而缺少时间维度的动态信息。

从群体行为的动态性角度出发，另一类模型研究人群移动的起止（Origin-Destination）规
律，即对给定空间内任意两点之间的人口迁移数量进行建模，本文称作``OD模型''。这类模
型利用地点对、以及周边环境的局部信息\supercite{palchykov_inferring_2014}对人口迁
移量进行建模。这里介绍三种典型的OD模型：干扰机会模型（Intervening Opportunity
Model, IOM）\supercite{stouffer_intervening_1940}，重力模型（Gravity Model,
GM）\supercite{cochrane_possible_1975}和辐射模型（Radiation Model,
RM）\supercite{simini_universal_2012}。干扰机会模型表述成，地点A向地点B迁移的人群
数量，正比于两地点的机会数量，反比于A-B之间的干扰机会的总量。重力模型表述成，地
点A和地点B之间的人口迁移量（无方向），正比于A和B的人口总量，反比于二者之间的距离，
因其最终的数学形式和牛顿的重力公式类似而得名。辐射模型是最新提出的、仅利用局部人
口数据对两地点之间的人口迁移量进行建模的方法，其在国家和城市尺度上都较重力模型的
性能较好。Palchykov等\supercite{palchykov_inferring_2014}用两地之间的通信量代替人
口数量、提出了类似于重力模型形式的预测模型。综上所述，OD模型虽然具有对人群动态性
刻画的能力，但是这种能力建立在地点的局部信息之上，因此缺少大尺度上的空间分布特点、
以及不同空间点上人群密度的相关性。这些不足构成了本文利用移动网络数据，对人群时空
分布特点以及依赖性进行研究的动机。


\subsection{用户参与行为研究进展}

在以人为中心的技术和服务模型中，用户体验（UX）是一个重要的考量方面。从用户角度来
看，一个成功的人机交互服务设计不仅能够激发人们即刻的使用兴趣，而且能够让人们保持
较为持久的黏着度。用户参与行为描述了人类在网络服务使用过程中表现出来的时空行为，
是人类行为规律在网络空间的一种表现形式。因其和用户体验测量、服务质量评估等有着密
切的联系，近年来也受到行为科学研究领域的重视\supercite{lehmann_models_2012,
  peterson_measuring_2008, dobrian_understanding_2011,
  shafiq_understanding_2014, balachandran_developing_2013}。例
如，O'Brian等\supercite{obrien_what_2008}将参与行为的概念引入到Web测量中，并提出
基于时间阶段的参与行为框架对一般的人机交互行为进行描述。另一方
面，Attfield等\supercite{attfield_towards_2011}从特性角度（如持久度）将参与行为分
解成不同的描述维度，并在每个维度上将理论描述和实际观测指标联系在一起。除此以外，
由于用户参与行为和移动行为具有相似的时空数据结构，将二者结合起来进行类比研究，不
仅可以验证时空行为模式挖掘、时空依赖性模型等的有效性，还能够对人类时空行为的内在
规律从不同应用角度进行理解。

用户参与行为的测量对于了解用户体验有着重要的价值。无论是商品服务、人工服务、还是
网络服务，服务提供者都会注重对用户体验的把握，而用户体验表现在用户的参与行为上。
我们首先对用户参与行为的相关测量方法和成果进行介绍。传统上的直接测量方法是主观调
研，通过问卷\supercite{ickin_factors_2012}或用户打分\supercite{_itu-t_1996}的形式
收集用户对于某项服务的直接评价，这样的方法一般数据质量高，但是数据规模小，单样本
的成本较高。另外一类方法是利用被动的方法，对用户参与服务的过程进行客观测量。通过
提取、分析相关的测量指标，简介获得用户在服务过程中的体验。这样的方法在计算机游
戏\supercite{febretti_usability_2009}、Web服务分
析\supercite{lehmann_models_2012, peterson_measuring_2008}、和视频服务体验感
知\supercite{dobrian_understanding_2011, shafiq_understanding_2014,
  balachandran_developing_2013}等领域获得广泛应用。本文将这种被动的测量思路引入移
动体验测量中，进而对移动用户的参与行为进行量化分析。

随着无线技术的普及，商用WiFi网络和高速移动网络为研究移动用户的参与行为提供了较好
的平台。Afanasyev等\supercite{afanasyev_analysis_2008}利用Google的商用WiFi网络数
据，从时间动态性、空间异构性角度研究了用户服务流量和移动行为的内在模
式。Trestian等\supercite{trestian_measuring_2009}将用户的服务参与行为和空间移动行
为进行关联研究，发现移动模式和服务兴趣有着显著的关联，如驻留用户和移动性较强的用
户倾向于使用更多的服务类型。同时，由于地点类型和场景因素的影响，特定地点（如休闲
场所）和特定类型的网络服务关联在一起，表现了用户地点偏好对网络服务偏好的映
射。Gember等\supercite{gember_obtaining_2012}分析了在不同硬件平台上（移动和非移动）
用户的使用状态和物理空间环境对感知到的网络性能的影响。虽然这些成果对移动网络中用
户的参与行为进行了刻画，但是面临着两方面的不足：1）网络因素和场景因素较多，对用户
参与行为的影响也各有不同，孤立的因素分析并不能捕捉不同因素之间的相互作用。2）虽然
已有成果对用户的参与行为进行了量化分析，但是缺少对量化关系的模型建立，进而降低了
研究成果在其他应用中的复用程度。

\subsection{进展总结及分析}

\begin{figure}[!tb]
 \centering
 \includegraphics[width=0.75\textwidth]{chap1/BehaviorMining.pdf}
 \bicaption[fig:chp1:behaviormining]{基于数据分析流程的时空行为挖掘成果总结} {基
   于数据分析流程的时空行为挖掘成果总结} {Fig}{The refined framework of mining
   spatio-temporal human behaviour.}
\end{figure}

最后，从数据分析流程的角度，我们对时空行为挖掘的已有成果进行可视化分析。如
图\ref{fig:chp1:behaviormining}所示，行为数据分析通常包括\emph{数据采集和预处
  理}$\to$\emph{数据特征分析与刻画}$\to$\emph{理论模型建立}三个阶段，其中每个阶段
都包括行为分析（图\ref{fig:chp1:behaviormining}左侧/绿色部分）和质量控制（
图\ref{fig:chp1:behaviormining}右侧/黄色部分）两个方面的内容。可以看出，如果时空
行为挖掘仅仅重视分析方法和现象的展示，极易忽略对行为数据质量、现象显著性、以及模
型有效性的控制。具体而言，在数据采集和预处理阶段，数据修复的好坏决定了后续行为表
征与真实用户行为之间的差异大小，而数据置信度的测量有助于研究人员从宏观上对数据质
量有所把握，从而对挖掘结果的显著程度有所预判。在数据特征分析阶段，分析现象的显著
程度也应该具体而客观地衡量，以帮助研究人员判断数据现象是偶然所致，还是时空行为的
普遍规律。在理论模型建立之后、投入领域应用之前，应该经过不同场景下的数据集的验证。
由此可见，在行为科学领域，我们不但要不断发展行为挖掘的算法性能、模型准确度，还应
该在分析结果的质量控制上开发出更加有效的方法和工具。

\section{人类时空行为挖掘中的关键研究问题}

人类时空行为挖掘的广泛应用，标志着高效的模式挖掘算法、准确的行为模型、以及坚实的
理论支撑，有着重要的研究价值。本文基于将时间和空间维度的信息统筹考虑的思路，首先
分析了在移动大数据背景下，进行时空行为挖掘所面临的多重挑战，然后从中总结出关键研
究问题，并针对性地提出解决这些问题的思路和方法。

\subsection{面临的挑战}
\label{sec:intro:challenges}

虽然移动大数据为时空行为挖掘带来了便捷，但这种便捷同时伴随着新的挑战:

1）\emph{数据质量影响了时空行为分析算法和模型的性能}。在移动网络数据的采集过程中，
通常采用被动采集（Passive Collection）的方式，在最小化对在线业务的影响前提下，实
现采集样本数量的最大化。但是通常一个网络中有多种类型的组件，组件间又有着复杂的交
互行为。其中任何组件的宕机、错误，都有可能导致原始数据的质量降低，进而影响到时空
行为挖掘算法的性能\supercite{coscia_optimal_2012, tanahashi_inferring_2012,
  wesolowski_commentary:_2014}。如果底层数据质量不高，上层的数据分析算法和模型便
失去了可靠的基础。产生这一挑战的根本原因在于，通常的数据分析算法对数据质量作了较
为理想的假设，例如认为数据点的质量是均匀分布
的\supercite{schneider_unravelling_2013}。虽然这样的简化有利于降低算法的复杂度，
但是从另一个角度来讲，也减弱了算法对异常观测的容错能力，即鲁棒性。综上所述，在时
空行为研究当中，数据质量问题对数据采集、数据清洗、行为分析、以及行为建模等环节的
鲁棒性提出了更高的要求。

2）\emph{时空行为模式的挖掘算法缺少多维度特征的关联关系}。个体时空行为的差异，不
仅体现在空间停留位置的变化，还体现在人们在不同时间段拥有不同的出行计划。例如，在
空间上，出租车司机的出行轨迹和普通上班族有着较大的不同；在时间上，人们在工作日和
周末的出行方式和目的地也有着较大的差异。在传统行为模式挖掘中，序列模
式\supercite{tiakas_searching_2009, jeung_trajectory_2011,
  gong_trajectory_2013}和移动模序\supercite{schneider_unravelling_2013}算法，分别
从空间位置序列和无权重的轨迹拓扑角度，揭示了个体时空行为的共同特征。但是，这两类
算法由于缺失时间维度的信息，所表达的人类行为规律具有一定的局限性。在实际应用中，
除了时间和空间特征，用户的时空行为与行为发生时的现实场景有着密切的联系。通常场景
信息体现在位置周边区域的兴趣点（Point of Interest, POI）的分布上。例如，城市商业
区的POI类型繁多，包括大型商场、电影院、各类消费设施等；而住宅区的POI类型以居民小
区、学校、周边超市等生活服务设施为主。一个区域的兴趣点分布不同，人们在该区域的移
动行为往往具有较大的差异。由以上分析可见，在时空行为模式挖掘中，不仅要时间和空间
上的关联性，还应对周围场景因素的影响作系统性的量化分析。

3）\emph{时空行为的挖掘结果缺少对可靠性的客观、有效的评估}。在一个测量系统中，我
们通常不仅要获得观测的量值，还需要对测量值的可靠程度进行后验式评估。这样当使用这
些观测值作为另一个系统输入的时候，人们便能够有迹可循，根据输入的可靠性对输出结果
的误差进行估计。与此同理，无论是从时空数据中得到的行为模式，或是行为模型，当进一
步应用所得结论的时候（如利用行为模型进行新型移动协议的开发），我们都需要了解所采
用的行为模式或模型的可靠性如何。这个性质具有重要的意义，然而在以往的工作中较为缺
乏，因此近年来受到行为科学领域越来越多的重视\supercite{cuttone_inferring_2014,
  song_limits_2010, deville_dynamic_2014}。总体来看，以往工作中缺乏的主要原因来自
三个方面: i) 缺乏质量一致的多空间尺度下的时空行为数据集。多尺度的数据集有利于对行
为挖掘结果进行横纵向比较，但是由于大规模的网络数据掌握在运营商手中，进行不同尺度
的数据采集、尤其是在长时间尺度上，本身具有较大的挑战。ii) 缺少鲁棒性较高的个体移
动行为模型。人类的移动行为在不同尺度下观测，既有相似的、微观的移动行为模式（如移
动序列模式\supercite{jeung_trajectory_2011}），也有相异的、宏观的统计特征（如行为
间隔时间的分布\supercite{barabasi_origin_2005}）。结合合多空间尺度的时空数据进行
研究，便需要能够将微观模式和宏观统计特征在统一的框架下进行分析，而不是当前形成的
两股不同的研究力量。但是将微、宏观进行统一，便需要能够兼具二者核心特性的个体行为
模型。iii) 对时空行为挖掘的理论推导和实证研究不足；将时空行为的分析结果和已有的行
为科学理论统一起来，不仅使得时空行为的分析结果更加完备，而且为分析方法的普适性提
供了保障。

\subsection{关键算法研究}

自动化的时空数据挖掘和用户行为分析离不开高效的程序算法，本节对人类时空行为研究中
的关键算法进行分析和介绍：

1）\emph{时空数据的质量评估和提升算法}。在挑战分析中，读者对数据质量的重要性有了
初步的理解，这里对时空数据质量的算法作进一步介绍。时空数据质量的问题归根结蒂来源
于数据层面上，行为在时间和空间上的不连续性。通常受到移动设备电量的限制、或用户隐
私保护的考虑，数据采集模块（如GPS）并非持续、永久在线运行，而是进行特定时间间隔的
采样。时间的不连续性导致用户位置在没有数据记录的时间内是不确定
的\supercite{song_limits_2010}，从而造成数据记录的准确度降低。另一方面，由于手机
蜂窝网基站的覆盖范围通常在500m$\sim$2km之间，在移动网络数据中，即使用户在某时刻有
数据记录，用户在空间上的位置精度也受限于单个基站的覆盖度。由此可见，时空数据质量
算法的核心，便是通过量化的手段对数据的时间和空间不连续性进行客观概括。而时空数据
质量提升算法是对质量评估算法的补充，在对数据时空特征度量的基础上，对缺失和错误的
数据记录进行补充和修复。这类算法在数据样本极为稀少的情况下尤为重要。数据质量的提
升算法，一方面可以利用行为数据的总体或局部统计特征进行插值处理，但是处理的结果趋
于平滑，失去了较多的细节信息；另一方面也可以基于个体或群体的行为模式进行增强，这
样的处理结果保留了用户行为的个性化信息，但是对算法设计和性能提出了更高的要求。这
部分内容将在后续的第\ref{chap:dq}章作进一步阐述。

2）\emph{移动网络数据中的用户行为识别算法}。移动网络数据通常采集自底层设备信息，
如系统维护日志、原始网络流量等，导致所采集到的数据与上层用户的行为距离较远。这
里``距离''指用户的原始操作行为，经过多层软件和硬件的处理到达网络数据层，使得用户
的行为信息变得极其稀疏。以手机浏览网页为例，当用户点击一个超链接时，设备在向主服
务器请求网页文本的同时，也会从其他服务器获取网页的内嵌内容，如图片、广告、分析报
告等。这意味着原始的用户行为（即一次点击），被分割到不同的网络数据包和网络流上。
从网络流量中识别用户的这种服务参与行为，需要算法从原始移动网络流量中，检测出用户
使用网络服务时的独立点击行为、以及相应的网络状态和场景因素。用户参与行为识别算法
的挑战，不仅在于网络流量中行为信息的稀疏性，还在于移动大数据样本量大、数据传输速
率高带来的处理瓶颈，这便需要在算法性能和复杂度之间取得较好的平衡。因此高效的用户
行为识别算法，对移动用户的时空行为分析起到至关重要的作用。这部分内容将是
第\ref{chap:engbehave}章研究工作的基础。

3）\emph{包含时空特征的个体行为模式挖掘算法}。\ref{sec:intro:challenges}节介绍了
多维度特征对于时空模式挖掘的必要性。在利用程序对行为模式进行自动化处理时，需要在
算法设计上实现时间、空间、以及场景特征（如POI类型）的``有机''融合。在大多数的传统
模式挖掘算法\supercite{patel_incorporating_2012, giannotti_trajectory_2007,
  chen_constructing_2014}中，用户的行为模式表示为带有时间戳的符号序列。这类算法的
特点是将用户的空间转移行为看作马尔可夫过程，下一次停留地点受最近的停留地点所决定，
而时间仅仅以附加权重的形式出现，导致时间和空间的交互信息的缺失。尽管时间和空间表
征了用户行为的不同维度，但是二者之间的交互作用已经得到了实验的证实， 如不同时段人
们的通勤行为\supercite{jiang_discovering_2012}，以及时间段和网络服务偏好之间的关
联\supercite{trestian_measuring_2009}。同时包含时间和空间特征，并且将二者有机融合
在一起的算法，我们称作时空耦合的模式挖掘算法。这类算法旨在通过将时间特征（如停留
时间）和空间特征（如转移距离）同等对待，结合二者之间的依赖信息，提取出个体行为的
时空模式。算法设计的核心，是如何实现时空信息的``有机''融合，这需要对人类行为的时
空规律有更深层次的理解和刻画。在第\ref{chap:istpat}章和第\ref{chap:engbehave}章中，
我们介绍了两种不同类型的发现时空耦合的行为模式算法。

\subsection{关键分析与建模研究}

时空行为挖掘通常有两类目标，一是利用新型的数据源，从前人未曾观察过的角度，对个体
或群体的行为特征进行探索，进而对人类的行为规律进行解读。二是在需要以人类行为模型
为基础的研究（如新型移动网络协议开发）中，利用第一类研究中得到的行为规律，对人类
的时空行为进行模拟，从而在理论或实验环境下分析现实中的情形；甚至通过改变模型的参
数，构造出现实观测的可能变种，对所研究的问题分析得更加全面。从这两个目标出发，本
文总结出了时空行为挖掘中的三个关键子问题：

1）\emph{时空行为的量化分析}。在统计分析理论中，大多数的理论和方法（如著名的大数
定理）都是在讨论，如何从有限的部分观测结果中得出可靠的分析结论。受益于移动大数据
中丰富的样本，人类的时空行为分析克服了小数据上的统计规律的偏差。但同时新的问题出
现，即如何利用客观、有效的指标对海量的时空记录进行总结，使得大量的观测数据变成可
重复使用、或者进行比较的信息。本文提出，人类的时空行为需要从三个层次进行有效的量
化，即\emph{时间动态性、空间异构性、以及时空关联性}。时间动态性表示某个观测值随着
时间的变化规律；而空间异构性指该观测值在空间上分布的均匀程度。在人类行为动力学中，
人们已经找到了多种有效的量化指标对其进行刻画（参见\ref{sec:review:ststat}节）。然
而，我们对于时空关联性的认识依然有限。和前两类指标相比，时空关联性反映了人类行为
的深层次性质，因为这需要结合人类在空间位置和时间范围的内在联系。除此以外，外在的
场景因素也是影响时空关联性指标的重要因素，例如，用户使用移动网络服务时，不仅受到
个人日程安排和停留场所限制，也受到客观的网络状态、用户性格、操作习惯等的影响。在
后续的第三至五章中，我们从不同观测粒度上（如个体和群体）对人类的时空行为进行量化，
并构成了理论分析和建模的基础。

2）\emph{时空行为模式的理论方法研究}。人类时空行为分析的潜在价值之一，是其分析方
法能够实现理论化，并推广到能够转化成\emph{类时空行为}研究的问题上。实现分析方法理
论化的同时，有利于从不同领域的时空问题中提取出共性部分，利用数学工具进行推演并使
之完备，从而从另一个层面上加深我们对时空行为的认知。以传统时空序列挖掘为例，虽然
人们在多年间发现了多种类型的人类时空行为模式\supercite{tiakas_searching_2009,
  giannotti_trajectory_2007, eagle_eigenbehaviors:_2009}，并发现利用行为模式能够
较好地对下一位置进行预测，但是，Song等\supercite{song_limits_2010}从信息熵角度对
时空行为的可预测性进行了研究，从理论上给出了任意的时空序列预测算法性能的上限和下
限。这样的理论方法不仅使时空序列的研究系统化，而且对实际应用具有很强的指导意义。
在时间和空间融合的人类行为研究中，系统化的理论研究尚不充分，而本文着重探索了时空
行为理论方法中的时空耦合模式分析、多维度量化指标之间的结构化分析等，这些内容将在
第三和第五章中进行展开。

3）\emph{基于时空模式的行为模型研究}。在对人类时空行为进行量化和理论分析的基础上，
建立行为模型有助于将挖掘结果应用于实际场景中，如优化城市内的交通管理。但是，选择
从个体粒度还是群体粒度建模，模型的特点和适用场景是有所差异的。对于个体模型而言，
由于在大规模研究中个体数量多、各自之间的差异大，因此模型一方面要包含个体的个性化
特征，另一方面又需要在统一的模型中对不同个体进行描述。一般来讲，模型对个性化特征
描述得越准确，模拟生成的用户行为就越接近实际观测，对多样性的表达能力就相应地变弱，
所得的模型通常也较复杂。相反地，模型对不同个体之间的共性信息越多，鲁棒性便越高，
模拟生成用户的总体分布和实际便越接近，而个体化信息的损失就越大。同时做到这两方面
是比较困难的，因此需要在相互制约的两个因素之间寻找到平衡点。本文在第二章就介绍了
一种新型的平衡二者的个体行为模型。对于群体行为模型而言，人群在空间上的分布特征、
以及时间的动态变化是其核心因素。虽然已有的群体行为模
型\supercite{michalopoulou_towards_2011}对空间的分布特征能够较好地捕获，但是缺少
时间上的动态变化信息。另一方面，OD模型\supercite{simini_universal_2012}刻画了移动
网络上两点之间的人群迁移量，虽然这类模型包含了人群的动态变化，但主要是网络链路上
的局部动态信息，缺少全局特征，例如空间范围内的人群分布的关联关
系\supercite{daqing_spatial_2014}。针对以上不足，本文第四章提出了一种融合了空间分
布和时间动态性的群体移动模型。

\section{本文的主要研究工作和创新点}

面对时空数据挖掘的诸多挑战，本研究基于\emph{以多空间尺度数据为支撑、时空特征关联
  为核心、理论方法为保障}的解决思路，对人类时空行为进行了系统性的量化分析和理论建
模研究，内容框架如图\ref{fig:chp1:constributions}所示。具体而言，主要创新点包括：
\begin{figure}[!tb]
 \centering
 \includegraphics[width=0.95\textwidth]{chap1/contributions.pdf}
 \bicaption[fig:chp1:constributions]{本文主要研究内容及创新点概括} {本文主要研究
   内容及创新点概括} {Fig}{The diagram of key contributions in this work.}
\end{figure}

1）\emph{基于多空间尺度的移动网络数据，提出一种时空行为数据质量的评估和提升方法}。
该方法基于时空数据的一般特点。从单数据点、单用户样本、和群体观测角度，对数据质量
进行客观量化。首先，针对单数据点在时间和空间上离散的特点，分别从静态的时空分辨率
和动态的转移过程对数据采集的质量进行评估，从而有利于对不同数据集、以及同一数据集
的不同时刻进行比较分析。接下来，从单数据点的质量指标引申出用户轨迹的数据采集质量，
从时空异质性角度对单用户的轨迹质量进行量化，并和传统信息熵的度量进行了比较。最后，
从群体观测角度，我们基于用户的轨迹样本在空间上的分布特征，利用用户轨迹的质量指标
代替传统方法中的数据点规模，提出了针对弱数据质量的提升算法，并和传统的数据点规模
的评估方法进行了比较，其准确度得到了较大提高。详细内容参见第\ref{chap:dq}章。

2）\emph{从网络结构出发，提出了个体移动行为的介观模式，并对介观模式的提取算法、实
  证分析、以及一种新型的个体移动模型进行了系统性研究}。本文对传统个体微观序列模
式\supercite{patel_incorporating_2012, chen_constructing_2014}和宏观统计模
式\supercite{song_modelling_2010, brockmann_scaling_2006}进行了扩展，结合个体移动
的网络拓扑和时空属性特征，提出了个体移动行为的介观模式（Mesostructure）的概念。从
而将网络分析方法引入到个体时空行为的研究当中，为探索人类行为规律提供了新的视角。
为了从大量原始记录中提取介观模式，我们提出了拓扑和属性结合的图相似匹配算法TACSim。
结合不同个体行为之间的相似性，进而提出一种带有修剪技术的显著介观模式提取算法PPM，
实现了对用户群组的介观模式提取。利用城市尺度的观测数据，对介观模式进行了实证分析，
并和传统的移动模序分析\supercite{schneider_unravelling_2013}进行了比较。在介观模
式的自距离分析中，我们发现了介观模式的自距离与移动行为的结构异质性紧密相关，并表
现出四种相关关系，即零模式、对数模式、线性模式、以及随机模式。最后，基于得到的介
观行为模式，提出了一种鲁棒性更好的个体移动模型，即流涌现模型FEM。由于该模型建立在
机遇资源的空间分布和干扰机遇的框架\supercite{stouffer_intervening_1940,
  simini_universal_2012}之上，从而摒弃了传统模型中微观和宏观统计一致的假
设\supercite{brockmann_scaling_2006}，为连接微观移动模式挖掘和宏观统计分析提供了
基础。详细内容参见第\ref{chap:istpat}章。

3）\emph{同时考虑空间分布与时间动态特征，对不同空间尺度下的群体时空行为进行实证分
  析和建模研究}。本文利用三种不同空间尺度（校园、城市、国家）下的移动网络数据，对
人群在较大尺度下表现出来的``潮汐效应''进行了实证研究。首先，我们利用协方差方程对
群体的时空依赖关系进行描述，分别从时间和空间维度对群体行为的统计特征进行度量。这
样的建模方法既包含空间上的分布特征，也包括时间上的时律性。我们发现，国家尺度上的
资源分布和校园尺度上的群体构成，对人群时空分布具有相似的影响，而城市尺度上的区域
功能差异则表现出不同的影响特征。在较大空间尺度下（如城市和国家），人群聚集度较高
的区域动态变化范围反而相对较小，其原因在于人群移动的 “莱维飞行” 特性更加突出，
倾向于以非常小的概率进行长途的城际旅行。基于所观测到的群体时空关联关系，在考虑空
间不同区域差异性的前提下，本文提出了基于盖内特分布的群体行为模型，并利用城市尺度
下的人群分布预测对模型性能进行了验证和分析。实验结果证明，融合了时空关联信息的模
型，在不同观测时间段内均表现出较好的预测性能，且预测准确度提高了
约3.7\%$\sim$23.6\%。详细内容参见本文第\ref{chap:gstpat}章。

4）\emph{提出一种被动的用户行为识别方法，对用户参与行为进行结构化分析，并结合场景
  因素进行建模研究}。本文将物理空间的移动行为和网络空间的参与行为在形式上进行了统
一，利用服务类型序列代替空间位置序列，对移动用户的参与行为模式进行挖掘。针对移动
用户参与网络服务的时空行为，提出一种基于被动测量的行为识别算法$\mathcal{AID}$。该
算法充分利用网络访问请求之间的逻辑约束条件，克服了移动网络流量引用关系缺失的挑战。
通过与客户端采集的基准数据进行比较，算法的识别准确度比已有的流结构算法提高
了10\%以上。通过量化用户参与行为的重要指标，建立了参与行为和底层网络性能之间的联
系，展示了不同硬件平台下用户参与行为随网络性能之间的变化关系。进而提出了一种结构
相关性分析（Structured Correlation Analysis）的方法，对场景因素（即用户个性、应用
类型、地点熟悉度等）如何用户的参与行为进行了细粒度的量化分析。最后，基于对参与行
为时空特性的研究，提出了利用隐马尔可夫过程的参与行为建模，并对群体参与行为进行了
聚类分析。详细内容参见本文第\ref{chap:engbehave}章。

\section{本文的结构安排}

本文主要内容结构安排如下：第\ref{chap:dq}章首先介绍了本研究使用到的多种来源、不同
空间尺度上的移动网络数据集，并对网络流量的采集处理平台、用户行为识别算法进行了介
绍；结合提出的数据质量管理框架，对本文使用的时空行为数据集质量进行分析和比较。
第\ref{chap:istpat}章对个体移动行为进行研究，介绍了新型的个体介观模式挖掘算法，以
及和传统模序分析的比较；基于介观模式提出了一种鲁棒性更好的个体移动模型。
第\ref{chap:gstpat}章作为对移动行为分析的延续，从群体角度对不同空间尺度上的时空分
布特征进行了研究，并对群体的时空依赖性建立了统计模型。第\ref{chap:engbehave}章将
多维度融合分析的思想应用在用户参与行为上，在对用户参与行为进行量化分析的基础上，
建立了相应的参与行为模型。第\ref{chap:future}章是对本文研究工作的总结、及未来研究
方向的展望。
