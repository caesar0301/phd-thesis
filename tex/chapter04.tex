%# -*- coding: utf-8-unix -*-
\chapter{群体移动行为的时空分布研究}
\label{chap:gstpat}

\newcommand{\varsp}{\mathbf{s}}

前一章从个体介观模式角度揭示了人类时空行为具有普遍的时空依赖关系。当我们去掉个体
差异、从更大的空间尺度上观测时，用户的群体行为同样也表现出较强的时空关联。例如在
城市的交通网络中，由于区域功能的差异性，人群的分布和移动行为具有不对称的特点，上
下班高峰路段拥堵严重，且随着时间的推移，这种趋势沿着道路网络不断传播。实际生活中
的类似现象，来源于群体移动行为本身在时间和空间上并不完全独立，而是相互依赖，相互
影响。但是在移动网络技术普及之前，群体行为的研究依赖人口普查机构的调研数据，即使
这类数据覆盖的空间范围可以很广（如芝加哥旅行调研数据\footnote{芝加哥旅行调研数据：
    url{
      http://www.cmap.illinois.gov/data/transportation/travel-tracker-survey}}），
但是由于样本数较小，且用户记录和描述的误差较大，为测量和分析群体移动行为的时空关
联带来了挑战。本章借助于被动的移动网络流量采集，从不同空间尺度，即校园（WIFI-M数
  据集）、城市（CITY-M数据集）、以及国家（Senegal-S和Senegal-A数据集）尺度上对群
体行为的时空关联进行分析和建模。通过对不同尺度上群体行为的对比研究，发现了人类行
为中时空关联的一般规律，进而对群体行为时空规律的产生机理建立了理论模型，并通过时
空预测的方法对模型进行了验证。该研究的成果可以在多个领域得到应用，如在移动网络部
署中，最优的基站规划需要考虑所在地点的人群移动特点，从而以最小的资源投入获得最大
的用户体验；在网络仿真领域，群体的时空分布特点有助于开发出更加真实的网络流量模型，
为新协议的开发提供了便捷。

\section{群体行为的研究背景}

群体性的时空行为在自然和人工领域有着丰富的形式，如自然界里的动物季节性迁徙、植被
覆盖范围的变化，人类社会中的移动网络用户移动、交通网络流量的变化、以及供电网络中
的用电量变化等。与个体粒度的行为分析不同，群体行为的研究帮助人们从宏观尺度上把握
复杂系统的动态变化，在对系统变化规律理解的基础上，反过来产生更有效的系统设计和管
理策略。本文以此为出发点，从第\ref{chap:istpat}章观测到的个体时空模式中引出群体
移动行为的时空依赖性，并在多空间尺度下对群体移动行为进行了实证分析和理论建模研
究。

\begin{figure}[!tb]
 \centering
 \includegraphics[width=0.95\textwidth]{chap4/ind-group-cmp.pdf}
 \bicaption[fig:ind-group-cmp]{个体和群体行为的时空分布特征对
   比}{个体(a$\sim$c)和群体(d)行为的时空分布特征对比} {Fig}{The comparison of
   investigating mobility from individual and group perspectives.}
\end{figure}

虽然群体由大量独立的个体构成，但是\emph{在时间和空间观测角度上，二者的特点和产生
  机理并不完全相同}。对于个体而言，行为上的时空依赖性更多来源于人们自身的差异和
偏好，如年龄、社会角色等。图\ref{fig:ind-group-cmp}中a$\sim$c展示了三种不同身份
的个体所产生的时空模式，其中节点代表不同的停留地点，大小和颜色分别表示累积停留时
间和同一个聚类簇，边方向和粗细分别表示移动方向和路径出现的频繁程度；可以看出个体
的行为分布决定于个体的移动能力、位置偏好、以及不同地点的时间需求的不同，如图中出
租司机较普通上班族表现出更加复杂的时空结构。对于群体而言，由于空间尺度远大于单个
个体的活动范围，行为的时空关联决定于地理空间上的资源分布（如工作机会）和区域功能
上的差异（如市区和郊区、居住地和道路等）。图\ref{fig:ind-group-cmp}d展示
了Senegal-S数据集所描述的塞内加尔用户在一周内的时空行为分布，可以看出首都和其他
主要城市构成了分布图的主要节点，国家级交通网络和航班路线构成了主要的转移路径。由
于存在这些差异，基于个体介观模式揭示的时空依赖性，群体移动行为的研究需要从宏观尺
度上对时间和空间特点进行关联分析。

\begin{figure}[!h]
 \centering
 \includegraphics[width=0.8\textwidth]{chap4/space-time-solo.pdf}
 \bicaption[fig:space-time-methods]{不同角度的群体时空行为研究方法比较}{不同角度
   的群体时空行为研究方法比较} {Fig}{The comparison of investigating group
   mobility from spatial and temporal dimensions separately.}
\end{figure}

在一定空间范围内，个体的移动行为导致群体的时空分布不断发生着变化，那么\emph{人群
  在时间和空间上的特征是如何关联在一起的呢}？图\ref{fig:space-time-methods}展示
了三种不同的分析方法和思路。\emph{方法a}以时间为导向，具体来讲，对于不同的空间位
置，随着时间的推移特定空间点（如手机基站）产生一个独特的观测时间序列。通过对单个
时间序列的分析，能够把握每个地点的变化特点及趋势；通过对两个或多个时间序列进行关
联分析，便可以对地点之间的相互影响进行衡量。这种方法的优势在于纯时间序列的分析方
法较为成熟，一般处理效率也较高；不足之处在于虽然时间序列观测是空间移动的结果，但
是并没有包含显式的时空交互特征，尤其在不同的区域（如城区和郊区）这种交互特征有所
不同。\emph{方法b}以空间为导向，即在不同的时间片段里，人群移动形成了空间上的分布
特征。通过对相邻时间片段内的空间分布进行对比，能够得出不同区域之间人群变化的差异。
和方法a类似，这种方法的不足之处依然是缺少直接的时空依赖性信息。\emph{方法c}克服
了前两种方法的不足，即同时考虑群体行为在时间和空间上的相关性，如图所示地
点B在$t+1$时刻的观测不仅与自身$t$时刻的值有关，而且依赖于$t$时刻地点A的观测。该
方法立足于对群体移动过程的直接观测，具有显式的时空交互信息，因此是本节研究的主要
方面。下面对时空交互信息的来源和具体形式进行举例说明。

从形成机制上分析，群体行为的时空交互信息主要来自于两个方面：\textbf{网络效
  应}和\textbf{时律性}。首先，网络效应是空间依赖性的主要影响因素。在各种各样的人
造网络中，如移动网络、交通网络、以及电力网络，网络节点之间存在着较强的相关性，且
这种基本属性容易导致节点之间的级联失效。例如在交通网络中，路段通行量接近饱和极限
而发生拥堵，甚至瘫痪，从而将过剩的交通压力转移到空间关联的其他路段，并进一步使得
其他路段发生饱和而拥堵。以此类推，网络中某处的状态会随着拓扑关系而发生扩散，从而
形成不同地点之间的依赖关系。移动网络与此类似，一个基站的拥塞升高或体验下降同样会
对其他基站造成影响。另一方面，人类行为具有较强的时律性，在宏观尺度上表现为群体行
为的``潮汐效应''，即人群有规律地在不同的空间分布之间进行切换（如
图\ref{fig:space-time-heatmap}），而这样的分布通常由区域功能所决定，如城市中的住
宅区和商业区。当网络效应和时律性叠加时，例如移动网络基站人群的时律变化，会沿着移
动网络拓扑进行传播，进而与其他基站的时律变化进行叠加，使得群体行为呈现出较强的时
空依赖性。

\begin{figure}[!tb]
 \centering
 \includegraphics[width=\textwidth]{chap4/heatmap9hours.eps}
 \bicaption[fig:space-time-heatmap]{CITY-M数据集中群体行为的潮汐效应热力
   图}{CITY-M数据集中群体行为的潮汐效应热力图（06AM$\sim$22PM）}
 {Fig}{Demonstration of spatial-temporal distribution of cellular traffic at
   the city scale.}
\end{figure}

本章利用不同空间尺度下的移动网络数据，对用户在宏观尺度上体现出来的时空依赖性特征
进行分析，并对其形成过程进行理论建模研究。在研究过程中需要解决以下两个挑战：1）
人群分布值在时间和空间维度上都可以进行量化（如
图\ref{fig:space-time-methods}a和\ref{fig:space-time-methods}b），但是由于空间具
有各向异性，而时间没有，因此二者的变化值难以直接进行比较分析；2）时空关联分析中
需要同时考虑用户行为在时间和空间上的依赖性，从而增加了模型的复杂程度。尽管如此，
对群体移动行为的时间和空间关联特性进行研究，不但可以缩小仅考虑时间或空间因素的理
论模型和实际观测之间的差距，而且在网络调度和资源优化中具有重要的应用价值。例如在
移动网络中，移动信号的干扰和基站的距离有关，从宏观上同时考虑网络资源的空间配置和
时间动态性，能够实现对珍贵的频谱资源进行有效利用。

\section{群体行为的时空统计分析}

\subsection{群体行为的时空分布描述}

我们首先从过程角度对群体用户的时空分布进行描述。由于网络效应的存在，人群在时空上
的分布具有连续性，即距离较近的空间点往往具有较高的相似度。因此对人群的时空分布特
征进行描述，不仅要保持不同观测点的差异，还应该保留在观测维度上的连续性。具体来讲，
我们对时间和空间维度建立索引，并将人群在特定时、空范围内的分布进行量化。因此在某
个时间点上的人群数用标量随机变量进行表示，即$y(\varsp, t)$，其中独立的随机变
量$\varsp$和$t$分别表示空间和时间上的索引值，则生成该随机变量的统计过程由定
义\ref{def:stdist}给出。

\begin{defn}
  \label{def:stdist}
  \textbf{群体时空分布}：假设观测$y(\varsp, t)$由随机过程$Y(\varsp, t)$产生，我
  们将群体的时空分布过程定义为
  \begin{equation}
    \label{eq:stseries}
    \{ y(\varsp, t) \sim Y(\varsp, t) | \varsp \in D_s, t \in D_t \},
  \end{equation}
  其中$D_s=\{D_s^{(k)}:k=1,\ldots,N\}$和$D_t=\mathbb{Z}$分别表示观测的空间和时间
  范围。
\end{defn}

虽然\ref{eq:stseries}式中的$D_s$和$D_t$为连续的定义域，实际观测和分析中由于数据
源的限制，往往造成定义域离散化。在我们的分析中，时间域被等分为相同的时间间隔，并
对同一间隔内的人群分布特征进行统计汇总；空间域则被分割成两两不相交空间区块，每个
区块中的点对于整个该区块来说是等概率的。对于每个区域的空间坐标，如果区域内有单个
基站则记录为该基站坐标，否则为基站坐标的平均值。

本文中我们重点研究移动网络中人群分布的\emph{时空统计模型}。通常情况下，统计模型
有两种不同的类型：一种是基于动态的物理过程描述，对统计过程产生的因素进行量化和关
联，如附近地点对当前空间点的影响。这样的描述方法包含了底层过程的演化规律和因果关
系，通常建立在条件概率分布（Conditional probability distribution）之上，利用附近
地点的过去观测值
\begin{equation}
  \{ Y(\mathbf{x}, r): \mathbf{x} \in D_s, r \leq t\} \cup
  \{ Y(\mathbf{x}; t): \mathbf{x} \neq \varsp \}
\end{equation}
对当前地点的值$Y(\varsp, t)$进行估计。但是条件概率分布的得出，需要对物理过程的不
同阶段、以及不同部分有定量的描述，而这个条件在实际中往往难以满足。如在群体移动中，
某工厂区域对商业区的人群移动，不仅和两地区的人口分布有关，而且受到交通网络、人们
的生活需求等具体因素影响。对动态的物理过程建模，需要对不同因素、以及因素之间的影
响进行建模，而这在仅已知宏观观测数据的前提下是难以获得的。另一种方法是在过物理过
程的细节信息缺少的情况下，采用统计描述的方法，利用统计分布的一阶或二阶矩（如均值、
  方差、协方差等）特征对分布进行建模。与物理过程模型的条件概率分布不同的是，统计
描述模型基于边缘概率分布（Marginal probability distributions）。在物理过程模型已
知的条件下，我们也能够导出统计描述模型，因此统计描述模型具有更好的一般性和普适性。
因此，在本文中，我们采用第二种方法，利用移动网络数据研究群体的时空分布特征，以及
基于这些特征的统计描述模型。

\subsection{群体行为的时空相关性}

在移动网络中，不同基站或建筑附近的人群分布随着时间推移不断发生着交互，因此在时间
和空间上具有较强的相关性。我们用时空协方差函数（Spatio-Temporal Covariance
Function,
STCF）对不同时空点的人群观测进行描述。假设$Y(\varsp, t)$是一个在时间和空间上都具
有二阶矩的稳定随机过程，其期望和方差分别满足$E[Y(\varsp,
t)]=\mu$和$Var[Y(\varsp,
t)]=\sigma^2 < \infty$，给定两个时空点$(\varsp_i,t_i)$和$(\varsp_j,t_j)$，他们的时空协方差函数表示为：
\begin{equation} \label{eq:stcovar}
  C(\varsp_i, \varsp_j; t_i, t_j) = Cov[Y(\varsp_i,t_i), Y(\varsp_j,t_j)]
  =C(\mathbf{h}, u)
\end{equation}
其中$\mathbf{h}=\varsp_j-\varsp_i$和$u=t_j-t_i$分别表示空间和时间上的跨度（Lag）。
时空协方差函数描述了一个时空点的观测值对另一个时空点的影响信息，且协方差值越大，
表示二者之间的相关程度越强。给定零点的方差
值$\sigma^2=C(\mathbf{0},0)$，\ref{eq:stcovar}式的时空相关性函数表示
为$\rho(\mathbf{h}, u)=C(\mathbf{h}, u)/\sigma^2$。特别地，当空间或时间跨度为零
时，则得到时间或空间维度上的协方差函数。如$C(\mathbf{0}, u)$表示
图\ref{fig:space-time-methods}a中单个地点的观测序列在时间维度上的相关程度，
而$C(\mathbf{h}, 0)$描述了图\ref{fig:space-time-methods}b中单个时间片段上不同地
点之间的相关程度。

从理论上来讲，时空协方差函数为半正定函数，反之亦然。一个函数的半正定性由下面的
定义给出：
\begin{defn} \label{eq:ngdef}
  \textbf{半正定性}：对于定义在$D \times D$区间上的函数$\{
  f(h,u): h,u \in D \}$，其半正定性指对于任意复数$\{ a_i: i = 1, \ldots, m\}$，
  变量$\{u_i: i=1,\ldots,m\} \in D$，以及整数$m$，满足
$$
\sum_{i=1}^m \sum_{j=1}^m a_i \overline{a}_j f(h_i, u_j) \geq 0
$$
其中$\overline{a}_i$为$a_i$的复共轭。
\end{defn}

上述定义\ref{eq:stcovar}式揭示了时空协方差函数的一个重要性质，即时空稳定性。这里我们
给出时空稳定性的一般定义，即
\begin{defn}
  \textbf{时空稳定性}（Spatio-Temporal Stationarity）：定义在实数区
  间$\mathbb{R}^d \times \mathbb{R}$上的函数$f$为稳定时空协方差函数的\emph{充分
    条件}是：1）函数$f$是半正定的，2）满足关系$f(\varsp_i,
  \varsp_j; t_i, t_j) =C(\varsp_i - \varsp_j, t_i -
  t_j)$，其中$\varsp_i, \varsp_j \in \mathbb{R}^d, t_i, t_j \in \mathbb{R}$。
\end{defn}

进一步，我们可以分别考虑空间和时间维度上的稳定性。如果时空协方差函数满足
\begin{equation} \label{eq:spacesta}
  f(\varsp_i, \varsp_j; t_i, t_j) = C(\varsp_i - \varsp_j; t_i, t_j)
\end{equation}
则称函数$f$具有\emph{空间稳定性}。类似地，如果时空协方差函数满足
\begin{equation} \label{eq:timesta}
  f(\varsp_i, \varsp_j; t_i, t_j) = C(\varsp_i, \varsp_j; t_i - t_j)
\end{equation}
则该函数称为具有\emph{时间稳定性}。

在我们的观测维度里，时间往往以标量的形式出现，且标量值的增大意味着时间的增长。但
是对于空间来说，即使在不考虑高度的情况下，我们依然需要两个坐标维度（如经纬度）对
空间点进行测量，因此以矢量的形式出现。矢量意味着空间具有方向性（或各向异性），
如\ref{eq:stcovar}式中的空间跨度$\mathbf{h}$。虽然矢量形式的空间跨度在理论分析中
具有方便的优势，但是在实证研究中，由于不同空间尺度下用户移动的物理环境不同，导致
难以对群体的时空规律进行横向比较。基于这样的考虑，我们在群体移动行为中引入空间各
向同性。如定义\ref{def:spaceisotropy}所示，空间各项同性意味着人群分布的差异在不
同观测方向上看遵循相同的规律或关系。

\begin{defn} \label{def:spaceisotropy}
  \textbf{空间各向同性}（Spatial
  Isotropy）：定义在区间$\mathbb{R}^d \times \mathbb{R}$上的时空协方差函
  数$f$具有空间各向同性指满足
  \begin{equation}
    f(\varsp_i, \varsp_j; t_i, t_j) = C(||\varsp_i-\varsp_j||; t_i, t_j)
  \end{equation}
  其中$\varsp_i, \varsp_j \in \mathbb{R}^d, t_i, t_j \in \mathbb{R}$。
\end{defn}

利用移动网络的观测数据，我们可以对经验的时空协方差值进行计算。假设随机过
程$Y(\varsp, t)$的一阶矩仅与空间相关（即在时间上具有稳定性），且二阶矩仅与
空间和时间的跨度有关，则经验的时空协方差通过下式计算：
\begin{equation} \label{eq:empcov}
  \hat{C}(h, u) =
  \frac{1}{|N_{h}|}\frac{1}{|N_u|}
  \sum_{N_{h}} \sum_{N_u}
  (Y(\varsp_i,t_i) - \hat{\mu}(\varsp_i))
  (Y(\varsp_j,t_j) - \hat{\mu}(\varsp_j)),
\end{equation}
其中$\hat{\mu}(\varsp_i)=\frac{1}{T}\sum_{t=1}^T
Y(\varsp_i,t)$，$N_{h}$表示距离点$(\varsp_i,t_i)$的空间跨度为$h$的所有时空点
$(\varsp_j,t_j)$的集合，$N_{u}$表示距离点$(\varsp_i,t_i)$的时间跨度为$u$的所有
时空点$(\varsp_j,t_j)$的集合。这里我们假设空间跨度$\mathbf{h}$与方向无关，则空
间跨度向量可以用标量代替，即$C(\mathbf{h}, u) = C(h, u)$，其中$h
\in \mathbb{R}$表示空间跨度的欧式距离。类似的，经验的时空相关性定义为$\hat{\rho}(h,
u)=\hat{C}(h,u) / \hat{C}(0,0)$。接下来，我们对移动网络中群体行为的时空分布特
征、以及时空相关性进行实证分析研究。

\section{多空间尺度下的时空分布特征} \label{sec:gstpat:multiscale}

这部分我们基于多空间尺度下的移动网络数据，对人群移动和分布特点进行分析和量化。其
中，WIFI-M数据集记录了某大学校园内人群在各建筑和活动场所间的移动行为，CITY-M数据
集从城市尺度上记录了用户在各手机基站之间的转移行为，Senegal-S和Senegal-A数据集在
国家尺度上分别从手机基站和行政区规划粒度上记录了人群的移动行为。我们分别对人群的
时空分布以及关联特征进行挖掘和分析。

\subsection{空间分布特征}

\begin{figure}[!tb]
  \centering
  \subfigure[国家尺度]{
    \includegraphics[width=0.49\textwidth]{chap4/senegal_heatmap.png}}
  \subfigure[城市尺度]{
    \includegraphics[width=0.48\textwidth]{chap4/hz_heatmap.png}}
  \subfigure[校园尺度]{
    \includegraphics[width=0.5\textwidth]{chap4/sjtu_heatmap.pdf}}
  \bicaption[fig:popdist3]{不同空间尺度下观测到的群体分布热力图}{不同空间尺度下
    观测到的群体分布热力图（下午2点）}{Fig}{Heatmaps of population
    distributions observed at vayring spatial scales (2:00 PM).}
\end{figure}

在研究人群分布的时空相关性以前，我们首先对其空间特征，
即\ref{def:stdist}式中$t=t_0$时的分布$Y(\varsp, t_0)$进行分析。
图\ref{fig:popdist3}给出了不同空间尺度下人群分布的热力图，其中颜色越趋近于红色表
示所在位置的人群密度越高。在国家尺度上（图\ref{fig:popdist3}a），人群热点对应于
塞内加尔的主要城镇区域以及道路网络，且人群密度和城市规模表现出较强的相关性，如人
口数分别居于前两位的首都达喀尔（Dakar）和中部城市图巴（Touba）。由于人类个体的移
动行为具有``莱维飞行''的特性，同时倾向于聚集在工作机会较多的经济发达区域，因此造
成了空间大尺度上的较强的不均衡性。另一方面，便利的交通为人群大范围移动提供了可能，
从而导致北部和中部的道路主干网周围拥有可观的人群分布。在城市尺度上（
图\ref{fig:popdist3}b），人群分布的不均衡性较国家尺度上变小，这是由于城市范围内
资源（如工作机会等）分布更加均衡，且发达的交通网络为个体长距离移动提供了便捷。但
是，城市内不同功能区域之间依然有所差异，如图中坐标[175, 355]及周边区域为杭州市主
要商业圈，而坐标[170, 340]及周边区域为住宅区之一；可以看出，城市内人群存在明显
的``潮汐效应''，随着时间的推移，人群对不同地理区域的依赖程度也在发生着相应的变化，
从而导致在不同功能区域之间的周期性迁移。在更小的校园尺度上，我们观测到用户群体在
特定区域内的分布、以及与功能建筑的关系，从而形成对国家和城市尺度上观测的补充。通
常大学校园内具有完善的生活设施和功能单位，从而在一定程度上包含了较完整的师生移动
行为。如图\ref{fig:popdist3}c所示，在工作日高峰时段，校内的图书馆、教学楼、以及
科研楼等聚集了大量的人群，这样的分布特点主要取决于个体的位置偏好，以及不同位置在
个体生活中所扮演的功能角色。综上所述，\emph{群体移动的空间分布决定于地理空间上的
  资源分布、区域功能、以及群体构成上的差异}，如国家尺度上的不均衡取决于资源分布，
城市尺度上的分布主要受区域功能的影响，而校园尺度上的差异主要来自于不同的群体组成。
尽管如此，不同尺度上观测的差异依然来源于个体的移动规律、以及与物理空间的相互作用。
为了进一步对这种分布特征进行研究，我们接下来从网络基站粒度上对人群分布进行量化研
究。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]{chap4/space3.pdf}
  \bicaption[fig:voronoi3]{不同空间尺度下网络基站/热点粒度下的人群分布}{不同空间
    尺度下网络基站/热点粒度下的人群分布（下午2点）}{Fig}{Comparison of
    population distributions observed at base-station granularity (2:00 PM).}
\end{figure}

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]{chap4/spatial_density_cmp.eps}
  \bicaption[fig:spdist3]{网络基站粒度下人群和网络流量的空间异质性对比}{网络基站
    粒度下的人群和网络流量空间异质性对比（下午2点）}{Fig}{The heterogeneity
    comparison of population and mobile traffic distributions（2:00 PM）}
\end{figure}

由于我们的数据采集点对应于离散的网络基站或热点，因此首先将连续的观测范围划分为不
重叠的空间区域，如图\ref{fig:voronoi3}所示，单个区域内的点到该区域中心的距离最短。
一方面，该图从基站/热点粒度上验证了上述人群分布与地理空间因素之间的相互关系；另
一方面，在不同的空间尺度下，可以看到高密度的人群分布聚集在较少数的基站/热点周围，
而大多数区域的人数较少，即表现出较强的\emph{空间异质性}（Spatial Heterogeneity）。
为了从量化角度捕捉这种异质性，我们对给定时间点的人群分布进行统计建模，这里考虑了
三种不同的重尾分布关系，即
\begin{itemize}
\item
  韦伯（Webull）分布：$f(x; \lambda, k) =
  \frac{k}{\lambda}(\frac{x}{\lambda})^{k-1}e^{-(x/\lambda)^k}, x \geq 0$；

\item
  对数正态（Log-normal）分布：$f(x; \mu, \sigma) = \frac{1}{x \sigma
    \sqrt{2\pi}} e^{-(\ln x - \mu)^2/2\sigma^2}, x \geq 0$；

\item 混合对数正态（Log-normal
  mixture）分布：$p(x) = \lambda_1 f(x; \mu_1, \sigma_1) + \lambda_2 f(x;
  \mu_2, \sigma_2)$，其中$\lambda_1
  + \lambda_2 = 1, x \geq 0$，且$f(x;
  \mu_1, \sigma_1)$为参数$\mu_1$和$\sigma_1$的对数正态分布；
\end{itemize}

图\ref{fig:spdist3}展示了人群在网络基站粒度下的经验分布以及上述统计拟合。可以看
出，与韦伯和对数正态分布相比，混合分布更能准确捕获经验数据分布，这是由于人群具有
明显的空间异质性，而这种异质性通过具有不同特征值的正态分布子模式体现出来，例如对
于塞内加尔，不同子模式的特征值分别为$y=2^2$和$y=2^4$，分别对应密度差异较大的两类
基站群。另一方面，同样对于混合对数正态分布，国家和城市尺度下有明显的双峰特点，而
校园尺度下较弱，这是因为人类移动性的限制，在较大空间范围内观测时人群分布的不均衡
性较强，而对于小空间的校园环境，个体之间的交互增多，群体结构上的差异并不明显，因
此表现出单模特征。此外，Michalopoulou等\supercite{michalopoulou_towards_2011}预
言在城市尺度上，出于对城市环境和网络业务的考虑，网络流量和人群分布的相关性并不显
著。我们同样对此假设进行实证分析。图\ref{fig:spdist3}中比较了杭州市在基站粒度上
的人群和网络流量分布，可以看出人群分布的双模特征比网络流量明显，即人群空间分布的
异质性比网络流量要高。这主要是因为，在移动网络中，用户的上网行为受空间约束较小，
即使拥有少数用户的基站，随着应用类型的的不同（如视频、音乐），也可能产生较高的流
量比例，从而降低了不同空间点上流量分布的差异。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.9\textwidth]{chap4/flowmap_graph.pdf}
  \bicaption[fig:groupmobility3]{不同空间尺度下人群移动的空间结构对比}{不同空间
    尺度下人群移动的空间结构对比}{Fig}{The comparison of mobility structures
    for group population at varying scales.}
\end{figure}

\begin{table}[!tb]\small
  \begin{center}
    \bicaption[tb:tplparams]{不同空间尺度下人群移动图节点度分布规律}{不同
      空间尺度下人群移动图节点度分布规律}{Table}{Parameter comparison of
      truncated power-laws over different spatial granularities.}
    \begin{tabular}{cccc}
      \toprule
      TPL参数 & 国家尺度 & 城市尺度 & 校园尺度 \\
      \midrule
      $a$ & 0.004 & 0.233 & 0.034\\
      $\lambda$ & -0.746 & 0.119 & -0.150 \\
      $k$ & 19.3 & 5.57 & 18.1 \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{table}

除了人群在空间上的静态分布特征以外，相同时间段内的不同地点的人群交互（即动态性）
在不同空间尺度下也有较大的差异。如图\ref{fig:groupmobility3}所示，我们生成不同空
间尺度下的人群移动结构图：其中每个节点表示不同的网络基站或热点，连接节点的边表示
对应地点对之间存在人群交互，且交互量用边的粗细进行标示。从图左侧部分可以看出，在
国家尺度上，人群移动在各大主要城市中形成了簇类结构，且最大的点簇表示塞内加尔首都
达喀尔，其他较小的独立点簇表示距离较远、人群交互较小的其他城市区域；在城市尺度上，
图中大部分节点处于连通状态，表示城市内部区域之间的关联紧密，不同区域之间的人群交
互较频繁；在校园尺度上，不同功能的建筑相互连通，其中中心度（Centrality）最高的节
点为少数的公共建筑区域，如图书馆、校园餐厅、活动中心等。从统计角度来看，
图\ref{fig:groupmobility3}右侧展示了不同个空间尺度下的节点度分布满足截尾的幂律分
布（Truncated Power-Law）规律:
\begin{equation}
  \label{eq:truncatedpowerlaw}
  D \sim a \cdot x^{-\lambda} e^{-\frac{x}{k}}, -1 \leq \lambda \leq 1.
\end{equation}
其中$\lambda$为幂律分布参数，$k$为截尾的指数分布参数。比较有趣的是，国家和校园尺
度上的节点度分布较为接近，且二者与城市尺度上的差异较大。如表\ref{tb:tplparams}所
示，国家和校园尺度上幂律分布参数分别为$\lambda=-0.746$和$\lambda=-0.150$，且截尾
参数$k$以及调节因子$a$也较为接近。这意味着，\emph{国家尺度上的资源分布和校园尺度
  上的群体构成差异，对人群时空分布具有相似的影响，而城市尺度上的区域功能差异则表
  现出不同的特征}。

\begin{figure}[!tb]
  \centering
  \subfigure[国家（AM）]{\includegraphics[width=0.32\textwidth]
    {chap4/senegal_spatial_correlation_pl_am.pdf}}
  \subfigure[国家（PM）]{\includegraphics[width=0.32\textwidth]
    {chap4/senegal_spatial_correlation_pl_pm.pdf}}
  \subfigure[城市（AM）]{\includegraphics[width=0.32\textwidth]
    {chap4/hz_spatial_correlation_pl_am.pdf}}
  \subfigure[城市（PM）]{\includegraphics[width=0.32\textwidth]
    {chap4/hz_spatial_correlation_pl_pm.pdf}}
  \subfigure[校园（AM）]{\includegraphics[width=0.32\textwidth]
    {chap4/sjtu_spatial_correlation_pl_am.pdf}}
  \subfigure[校园（PM）]{\includegraphics[width=0.32\textwidth]
    {chap4/sjtu_spatial_correlation_pl_pm.pdf}}
  \bicaption[fig:spcorr3]{不同空间尺度下的人群移动的空间相关性分析}{不同空间尺度
    下的人群移动的空间相关性分析（杭州）}{Fig}{Spatial correlation analysis of
    population distribution at varying scales.}
\end{figure}

最后，我们对不同空间尺度下人群分布的空间相关性进行分析。根据\ref{eq:empcov}式，
观测的纯空间相关性为$\hat{\rho}(h, 0)=\hat{C}(h, 0) / \hat{C}(0,0)$。我们利用不
同时间段内一个小时的观测数据计算空间相关性，其中最小空间跨度的变化量$\Delta h$根
据观测尺度的不同而有所不同（国家尺度为5km，城市尺度为0.05km，校园尺度为0.001km）。
图\ref{fig:spcorr3}中展示了不同空间尺度下的人群移动空间相关性分布，在上午和下午
我们分别选取了高峰和非高峰时段的两个小时进行对比研究。如图所示，在国家尺度上，空
间相关性满足截尾的幂律分布（\ref{eq:truncatedpowerlaw}式）；无论在上午和下午时段，
高峰期和非高峰期都具有明显的差异，且不同时段内具有类似的空间相关性。例如，高峰期
上、下午的幂律参数分别为$\lambda_{AM}^{(p)}=0.638$和$\lambda_{PM}^{(p)}=0.821$，
而非高峰期为$\lambda_{AM}^{(np)}=0.475$和$\lambda_{PM}^{(np)}=0.421$；这表明和非
高峰期相比，高峰期的空间相关性随空间跨度的增大而减弱较快。这是由于在高峰时段内，
主要城市区域的群体移动性较强，造成与其他人口稀少区域的空间异质性增大，进而相关性
随空间跨度的递减速度也增大。

城市尺度上则表现出与国家尺度不同的特征：首先，城市尺度的空间相关性满足幂律分布，
即
\begin{equation}
  \label{eq:powerlaw}
  \hat{\rho}(h, 0) \sim b \cdot h^{-\gamma}
\end{equation}
其中，$\gamma$为幂律指数；这表明城市尺度的人群分布和国家尺度类似，即具有空间
长相关性（Spatial long-range dependence）。对于上午时段，高峰期和非高峰期的幂律
指数分别为$\gamma_{AM}^{(p)}=0.719$和$\gamma_{AM}^{(np)}=0.943$；而下午时段，高
峰期和非高峰其的幂律指数为$\gamma_{PM}^{(p)}=0.709$和$\gamma_{PM}^{(np)}=0.758$。
同时，对于\emph{最大空间相关距离}（定义\ref{def:spcorrmcd}），高峰期为$h_{X}
\approx 5km$，而非高峰期为$h_{X}
\approx 10km$。从上述观测可以看出，和高峰期相比，非高峰期的空间相关性下降速度较
快，且具有较强的长相关性。这样的差异和观测城市中居民的移动状态紧密联系在一起：在城
市尺度观测，人们通常在高峰期向聚集度高的城区（最远跨度约为5km）进行社交或商业活
动，而在非高峰期返回较分散的住宅区（最远跨度约为10$\sim$15km），从而造成了不同
时段最大空间相关距离以及幂律分布的差异。
\begin{defn}
  \label{def:spcorrmcd}
  \textbf{最大空间相关距离}（Maximum Correlative Distance，MCD）指特定的空间跨
  度$h_{X}$，即当$h > h_{X}$时，人群分布的空间相关性$\hat{\rho}(h, 0)$的减小速度
  比幂律分布快。
\end{defn}

在校园尺度上，人群分布在空间上的相关性不再具有幂律特征，且高峰期和非高峰期不再有
明显的异同点，如图\ref{fig:spcorr3}e和图\ref{fig:spcorr3}f所示。综上所述，在较大
尺度上，人群分布的空间相关性具有明显的幂律分布特征，具体而言，国家尺度满足截尾的
幂律分布，城市尺度满足幂律分布，且高峰期和非高峰期在相关性下降速度和最大相关距离
上有所不同，而造成这种现象的主要原因在于大尺度上资源分布和区域功能的差异；在较小
尺度上，如校园环境，人群分布的空间相关性不再具有幂律分布，产生这种特征的一种潜在
原因是，在较小空间尺度上，人群的结构差异成为了决定空间异质性的主要因素，而这些因
素主要受人为规划的建筑分布的影响、比受人群动态移动的影响更大。

\subsection{时间分布特征}

这部分我们对不同尺度下人群分布的时间特征进行分析和研究。虽然人群分布具有空间异质
性，但是这种异质性随着时间的变化而发生改变，且不同尺度上观测变化的规律有所不同。
图\ref{fig:spdistcity}、\ref{fig:spdistcountry}、\ref{fig:spdistcampus}分别从城
市、国家、校园尺度上对空间异质性的时变特征进行了展示。首先在
图\ref{fig:spdistcity}a中，我们测量了人群分布的经验观测与拟合的三种不同的重尾分
布之间的K-S距离，及其随时间的变化关系。可以看出在所有时间段中，混合对数正态分布
都具有最小的K-S距离，即该分布能够较好地捕捉底层的人群空间分布特征。在时间方向上，
韦伯和对数正态分布在白天（7:00$\sim$16:00）的拟合误差较大，而晚上的误差较小；这
是由白天人群移动的动态性更高、空间分布的异质性更强导致的。虽然混合对数正态分布的
拟合误差随时间的变化幅度较小，但其在夜晚的性能有所降低，并处在与对数正态分布相当
的拟合误差范围内。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]{chap4/hz_spatial_density_mixture_mod.eps}
  \bicaption[fig:spdistcity]{城市尺度下人群分布的双模模型参数与时间的关系}{城市
    尺度下人群分布的双模模型参数与时间的关系（杭州）}{Fig}{The temporal
    tendency of model parameters for population distributions at city scale.}
\end{figure}

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]
  {chap4/senegal_spatial_density_mixture.eps}
  \bicaption[fig:spdistcountry]{国家尺度下人群分布的双模模型参数与时间的关系}{国
    家尺度下人群分布的双模模型参数与时间的关系（塞内加尔）}{Fig}{The temporal
    tendency of model parameters for population distributions at nation scale.}
\end{figure}

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]{chap4/sjtu_spatial_density_mixture.eps}
  \bicaption[fig:spdistcampus]{校园尺度下人群分布的双模模型参数与时间的关系}{校园
    尺度下人群分布的双模模型参数与时间的关系}{Fig}{The temporal tendency of
    model parameters for population distributions at campus scale.}
\end{figure}

混合对数正态分布$p(x) = \lambda_1 f(x; \mu_1, \sigma_1) + \lambda_2 f(x; \mu_2,
\sigma_2)$通过不同的模式捕捉具有差异较大的两组人群分布的空间特征，因此表现出较
好的平均拟合性能。我们对其两组组成模式的特征参数进行分析，即对数正态分布的期望
$\mu$和方差$\sigma$、以及调和因子$\lambda$。图\ref{fig:spdistcity}b展示了期望
$\mu$（准确地讲，该期望表示人群分布的特征值为$y=2^{\mu}$）随时间的变化关系。可
见在城市尺度上存在两种不同的人群空间分布模式，其中模式1包括聚集程度较高的城市区
域，如高峰期的商业区；而模式2包括其余人群聚集度小的区域，如高峰期的住宅区。由于
模式1中包含的区域人群移动性强，因此具有较大的方差（如图\ref{fig:spdistcity}c所
示）。更进一步，我们通过量化的方法测量出模式1的期望和方差特征值分别为
$\mu_1=2^{2.1} \sim 2^{2.7}$和$\sigma_1= 2^{0.9}\sim 2^{1.25}$，模式2的期望和方
差特征值为$\mu_2=2^{0.2} \sim 2^{0.5}$和$\sigma_2= 2^{0.25}\sim 2^{0.4}$。图
\ref{fig:spdistcity}d从概率角度展示了不同成分在经验观测中的比重，可以看出在所有
观测时段内，模式1覆盖了更大的观测范围（参考图\ref{fig:spdist3}）。

接下来，我们对国家和校园尺度上的人群分布模式进行了对比研究。
图\ref{fig:spdistcountry}首先展示了国家尺度上人群分布模式的期望和方差分布。可以
看出，与城市观测相同的是，国家尺度上由两种差异较大的模式构成；与城市观测不同的是，
这里模式1虽然具有较大的方差，但是期望值比模式2小，代表聚集性较小的人群分布区域。
具体而言，模式1的期望和方差特征值分别为$\mu_1=2^{1.5} \sim
2^{3}$和$\sigma_1=
2^{0.96}\sim 2^{1.18}$，模式2的期望和方差特征值为$\mu_2=2^{3}
\sim 2^{5}$和$\sigma_2=
2^{0.51}\sim 2^{0.65}$。这表明国家与城市尺度观测的最显著差异在于，\emph{人群聚
  集度较高的区域动态变化范围反而相对较小}，其主要原因在于大尺度上观测时，人群移
动的``莱维飞行''特性更加突出，倾向于在特定城市范围内活动，而以非常小的概率进行
长途的城际旅行。图\ref{fig:spdistcampus}展示了校园尺度上人群分布的模式特征。可
以看出，与大空间尺度（即城市和国家）不同，人群分布的模式特征不再有明显的聚类特
征，代之以不同程度的交叠。例如在上午8:00$\sim$10:00之间，人群分布模式具有相近的
方差和不同的期望，而在中午11:00$\sim$12:00具有相近的期望和不同的方差。这表明校
园尺度下人群分布的空间异质性较弱，群体结构上的差异并不明显，更加倾向于单模特征。
从时间维度上看，随着时间的变化，模式1的期望值呈递增趋势，而模式2呈递减趋势。这
表明晚上比白天的空间异质性强，这是由于白天人群的移动性较强，在校园内各区域之间
的活动较频繁，而晚上对地点的偏好选择更强，因此两种分布模式也更加明显。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=1\textwidth]{chap4/heatmap_regions.eps}
  \bicaption[fig:cityregions]{城市尺度下不同功能区域的空间结构示意图}{城市尺度下
    不同功能区域的空间结构示意图（杭州）}{Fig}{Visualization of spatial
    structures of different regions at city scale.}
\end{figure}

\begin{figure}[!tb]
  \centering
  \subfigure[所有区域]{\includegraphics[width=0.32\textwidth]
    {chap4/hz_spatial_corr_regions_series.eps}}
  \subfigure[商业区]{\includegraphics[width=0.32\textwidth]
    {chap4/hz_spatial_corr_urban.eps}}
  \subfigure[住宅区]{\includegraphics[width=0.32\textwidth]
    {chap4/hz_spatial_corr_rural.eps}}
  \bicaption[fig:citytemporalcor]{城市尺度下不同功能区的人群移动趋势和时间相关性
    对比}{城市尺度下不同功能区的人群移动趋势和时间相关性对比（杭州）}{Fig}{The
    comparison of population tendencies and temporal correlation at varying
    areas.}
\end{figure}

除了不同时间片段内的人群空间分布的差异以外，特定地点或区域在时间上的相关性也具有
明显的特征。这里我们对城市尺度下的不同功能区域和基站观测进行研究。首先，
图\ref{fig:cityregions}a展示了我们选择的城市中三个不同的空间区域：从功能上讲，区
域A覆盖杭州市的主要商业区域，包括大型商场和写字楼建筑。区域B和区域C分别表示不同
地理位置的住宅区域；可见杭州市是一个单中心结构的城市，住宅区域主要分布在商业区的
周边地区。图\ref{fig:cityregions}b和图\ref{fig:cityregions}c分别从基站粒度上展
示了区域A和区域B在14:00的人群分布特征，其中颜色越接近红色表示人群密度越大；可以
看出商业区域的基站密度远高于住宅区域。

对于给定的区域或基站，我们对其人群分布的纯时间相关性进行计算，根
据\ref{eq:empcov}式，时间相关性计算为$\hat{\rho}(0, u)=\hat{C}(0, u) /
\hat{C}(0,0)$。图\ref{fig:citytemporalcor}a首先展示了不同区域在一周内的时间观测
序列，可以得出两个直接结论：一是从短时间范围来看，验证了城市内人群分布的``潮汐
效应''，例如在晚上商业区域和住宅区域的人群分布形成了明显的互补；二是在长时间范
围内，不同区域均具有和人类生活习惯一致的周期性，且周末时段住宅区域的人群聚集明
显高于工作日。图\ref{fig:citytemporalcor}b和\ref{fig:citytemporalcor}c分别给出
了商业和住宅区域的时间相关系数$\hat{\rho}(0,u)$随时间的变化曲线。从总体上来看，
人群分布具有较强的时间长相关性（Temporal
long-range dependence）以及人类生活节律带来的周期模式。商业区和住宅区的时间相关
性，在时间跨度$u=12h$处表现出不同的特征（
如$\hat{\rho}_A(0,u)=-0.6$，$\hat{\rho}_B(0,u)=0$），而在时间跨度$u=24h$处却具有
相似的特征（如$\hat{\rho}_A(0,u)=0.76$，$\hat{\rho}_B(0,u)=0.6$）。这主要源自于
超吸效应下人群在不同区域的移动特点，例如在我们的观测中，商业区在每天中下午时段达
到人数高峰期，而在上午和晚上人数最少；住宅区却在一天的时间段内呈现出递增的趋势。
但是，从基站粒度上观测，人群分布的时间相关性并不明显，这主要由于我们的观测基于移
动网络数据，而用户使用手机的行为往往具有即席收发的特点。

综上所述，由于城市内不同区域的功能定位以及长期观测中的人群移动性差异，人群分布的
时间相关性和区域类型有着紧密的联系。这启发我们在对人群时空分布的建模中，不仅要考
虑其在空间和时间上的分布特点，还需要考虑不同功能区域之间的差异。下一节中，我们利
用本节观测到的时间和空间分布特点，采用统计建模的方法对人群分布进行理论和实证分
析。

\section{群体行为的时空关联建模}

虽然个体的时空行为模型得到了广泛的关注，但是宏观尺度下的群体时空统计模型依然未能
得到深入的研究。时空统计分析针对时间和空间维度上多变量的观测序列，在环境和行为科
学领域得到了较多应用。我们的目标是对\ref{eq:empcov}式所得的经验分布建立时空统计
模型，从而能够重现移动网络中群体分布在时空上的变化过程（如
图\ref{fig:space-time-methods}）。时空统计模型由于方便融合时间和空间上的边缘分布
信息，因此为理解群体移动的时空过程提供了有效途径。这部分我们利用时空统计的方法对
群体时空分布进行建模，和传统的时间序列模型相比，所提出的新模型融合了时空关联性特
征；然后我们通过群体分布的预测对模型性能进行了验证，并对比了不同时段和区域功能等
因素对预测性能的影响。

\subsection{无时空关联的行为模型}

在引入时空关联信息之前，我们首先介绍并分析一种简单的行为模型。该模型中，时间和空
间上的信息采用相乘或相加的方式相互作用，在相关文献中也称作时空分离模型
（Separable Models）\supercite{cressie_statistics_2011} 。虽然这类模型具有有限的
时空关联信息，模型结果往往与实际差异较大，但是该类模型构成了我们分析时空相关性的
基础。通过比较，我们可以从量化的角度，衡量时空相关性对群体移动预测性能的提高程度。

一般来讲，由于相乘（$\times$）和相加（$+$）包含了相同的时空信息量，这里我们采用
前者进行分析，即时空协方差函数表示为
\begin{equation}\label{eq:sepstmodel}
  C(\mathbf{h}, u) = \sigma^2 \rho(\mathbf{h}) \rho(u),
\end{equation}
其中$\rho(\mathbf{h})$和$\rho(u)$分别表示空间和时间维度上的协相关函数,其隐含条件
为$C(\varsp_i, \varsp_j; t_i, t_j)$分别具有空间和时间稳定性。由
于$\sigma^2=C(\mathbf{0},0)$，\ref{eq:sepstmodel}式只需对相应的相关函数建模。如
定义\ref{eq:ngdef}可知，时空协方差函数的充分必要条件是满足半正定性，因此所选择的
时间和空间的相关函数乘积也需满足该条件。根据\ref{sec:gstpat:multiscale}节所得的
空间和时间特征，我们考虑两类带参数的模型:
\begin{itemize}
\item 指数模型（Exponential Mode）：$\rho(x) = e^{-x}$，其中参数满足$x \geq 0$；
\item 柯西模型（Cauchy Model）：$\rho(x) = (1+x^\alpha)^{-\frac{\beta}{\alpha}}$，
  其中$\alpha \in (0,2], \beta > 0$，且$\geq 0$。
\end{itemize}

无时空关联的模型由于形式简单，且纯空间的矩阵处理或时间序列处理都较为方便，因此在
以往的研究中经常被采用\supercite{cressie_statistics_2011}。但是这类模型的缺陷也
很明显，即缺少在经验数据中可直接观测到的时空依赖性信息。如\ref{eq:sepstmodel}式所
示，乘数因子$\rho(\mathbf{h})$意味着$\rho(\mathbf{h}; u_1) \propto
\rho(\mathbf{h}; u_2)$，即不同时间片段里的空间相关性满足相同的性质和规律；同理
$\rho(u)$意味着$\rho(\mathbf{h}_1; u) \propto \rho(\mathbf{h}_1; u)$，即不同地
点的时间序列具有相似的自相关特征。而这样假设的不足可以从
\ref{sec:gstpat:multiscale}节的时空相关性分布中反映出来。

\subsection{融合时空关联的行为模型}

这部分我们基于时空关联信息对群体行为分布进行建模。建模方法不同，时空关联信息的融
合形式也有所差异。在物理过程模型中，时空关联信息往往体现在群体移动的过程描述当中，
例如，对于给定地点$\mathbf{s}_i$，不同地点$\mathbf{s}_j$的关联强度不同，因此能够
以较为清晰的规则形式添加在模型当中。但是在时空统计模型当中，其核心是对时空协方差
函数进行统计建模，而挑战在于对于模型半正定性的验证。本研究基
于Gneiting范式\supercite{gneiting_nonseparable_2002}和实证分析的结果，提出了一种
描述群体分布的时空行为模型。该模型通过对移动网络中用户移动的数据分析，得出时间特
征以及时空交互特征的统计规律，进而利用Gneiting范式在保证模型半定性的前提下，将时
间和空间特征进行关联融合。

对于时空协方差函数的半正定性，Cressie等\supercite{cressie_statistics_2011}提出利
用傅里叶变换后的频域特征进行判断。这种方法由于依赖傅里叶变换，因此仅对极少数的积
分收敛的函数有效。但是在实际分析中，人群分布的时空分布难以满足这样严苛的收敛条件。
针对这样的不足，我们采用Gneiting范式对半正定性进行判断，由于避免了傅里叶分解，从
而将函数范围扩大至更多的带参数的分布函数，也构成了我们对群体行为建模的基础。

我们首先回顾函数单调性判断。假设定义在$t \geq 0$上的连续函数$\phi(t)$，其为严格
单调函数的条件为$n$阶导数满足
\begin{equation}
  (-1)^n \phi^{(n)}(t) \geq 0
\end{equation}
其中$t > 0,
n=0,1,2,\ldots$。则Gneiting范式描述为：假设$\phi(t),
t \geq 0$是严格的单调函数，$\psi(t),
t \geq 0$为正定函数且其积分为严格单调函数，则函数
\begin{equation}
  \label{eq:gneting}
  \rho(\mathbf{h}, u) =
  \frac{1}{\psi(u^2)^{d/2}}
  \phi(\frac{||\mathbf{h}||^2}{\psi(u^2)})
\end{equation}
是有效的时空协方差函数，其中$d \geq 0$，且$(\mathbf{h}, u) \in \mathbb{R}^d
\times \mathbb{R}$。

在\ref{eq:gneting}式中，函数$\psi(x)$对时间分布特征进行建模，而函数$\phi(x)$对时
间和空间的关联关系进行描述。对于人群时空分布来讲，时间和空间交互具有特定的分布规
律；为了尽可能多地捕获人群特征，我们根据实证观测提出以下特征函数：
\begin{itemize}
\item \textbf{时间特征}：$\psi(x)=(1+x^\alpha)^\beta, \alpha,\beta \in (0,1]$；
\item \textbf{时空关联特征}：$\phi(x)=\exp(-x^\lambda), \lambda \in (0,1]$。
\end{itemize}
将上述两个特征函数代入\ref{eq:gneting}式中得到群体分布的时空协方差函数为：
\begin{equation}
  \label{eq:gneting}
  \rho(\mathbf{h}, u) =
  \frac{1}{(1+u^{2\alpha})^{\frac{d\beta}{2}}}
  \exp(-\frac{||\mathbf{h}||^{2\lambda}}{(1+u^{2\alpha})^{\beta\lambda}})
\end{equation}

\subsection{模型性能验证}

前面两节中，我们利用移动数据分析的时空特征，提出了无时空关联的模型（简称
为ExpModel和CauchyModel）和融合时空交互信息的模型（简称为ST-Model），其中前者作
为比较和分析的基准。对于每一个带参数的候选模型，我们采用期望最大
（Expectation–maximization）算法对模型参数进行估计，即最大化相应的对数似然函数$
\mathcal{L}(\Theta|\mathbf{O}) = p(\mathbf{O}|\Theta)$其中$\Theta$表示模型的参
数集合$\mathbf{O}$表示数据集中的时空观测值。

\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.75\textwidth]{chap4/prediction_machine}
  \bicaption[fig:predictmachine]{利用时空关联特征进行人群分布预测的模型示意
    图}{利用时空关联特征进行人群分布预测的模型示意图}{Fig}{A brief illustration
    of population prediction machine with spatio-temporal dependence models.}
\end{figure}

接下来，我们通过时空分布预测的方法对模型的性能进行量化和比较。由于群体行为的时空
协方差函数包含时空关联信息，因此预测模型性能的高低可以用来衡量时空协方差函数与实
际观测的相符程度。图\ref{fig:predictmachine}展示了利用时空协方差函数的预测器。具
体来讲，我们将观测区域划分为不重叠的块，并利用矩阵$\mathbf{M}_i$记录在时
刻$t_i$时的人群空间分布观测。随着时间推移，多个这样的观测矩阵形成了矩阵序列。在
模型训练阶段，我们利用观测序列$\langle M_i, \ldots, M_j \rangle$对模型参数进行估
计。在预测阶段，我们利用观测历史中不同时空点（如点A和B）的线性组合对未来时空点
（如点C）进行预测，即
\begin{equation}
  \label{eq:prediction}
  y(s',t')=\mu(s', t') +
  \sum_{i=1}^N w_i \cdot (y(s_i,t_i) - \mu(s_i, t_i))
\end{equation}
其中$s \in \mathbb{Z}_N$，$t \in
\mathbb{Z}_T$，期望函数$\mu(s,t)
= E(Y(s,t))$，不同历史观测点的权重$w_1,\ldots,w_N$由$Y(s,
t)$的时空协方差函数决定，这里我们取权重为协相关值。在时空稳定性的前提下，我们进而获得预测方差为
\begin{equation}
  \sigma(s',t') = \sum_{i=1}^N \sum_{j=1}^N w_i w_j C(h, u)
\end{equation}
其中$h=s_i - s_j$和$u = t_i - t_j$分别为空间和时间跨度。

利用上述模型，我们同时在时间和空间域上对群体行为的分布进行了预测。对于模型在时间
段内的预测值，我们利用均方误差（RMSE）对预测性能进行评估，即
\begin{equation}
  \label{eq:rmse}
  RMSE = [\frac{1}{NT}\sum_s\sum_t(\hat{y}(s,t) - y(s,t))^2]^{\frac{1}{2}}
\end{equation}
其中$\hat{y}(s,t)$和$y(s,t)$分别表示在时空点$(s,t)$上的预测值和经验观测值。均方
误差越小，意味着预测模型性能越好，因而相应的群体分布模型与实际则越接近。

\subsection{数据分析及结果}

这部分利用CITY-M数据集对群体时空分布模型的性能进行验证。为了区分空间因素对预测准
确度的影响，我们对城市内不同功能区（即商业区和住宅区）进行了分析；同时为了考察时
间因素的影响，我们对一周内的不同时间天进行了独立分析，从而获得了时间维度上时空关
联信息对群体分布建模和预测性能的影响。

\begin{table}[!tb]
  \centering
  \small
  \bicaption[tbl:rmsestat]{不同模型在考虑区域和时段差异下的预测性能对比}{不同模
    型在考虑区域和时段差异下的预测性能对比}{Table}{The RMSE statistics for prediction
    performance of different models.}
  \begin{tabular}{|c|ccc|ccc| }
    \hline
    \multirow{2}{*}{Day} & \multicolumn{3}{c}{Rural Area} &
    \multicolumn{3}{|c|}{Urban Area} \\ \cline{2-7}
    & ExpModel & CauchyModel & ST-Model & ExpModel & CauchyModel & ST-Model \\ \hline
    TUE & 0.182 & \textbf{0.165} & 0.181 & 0.180 & 0.198 & \textbf{0.166} \\ \hline
    WED & 0.208 & 0.158 & \textbf{0.156} & \textbf{0.186} & 0.192 & 0.198 \\ \hline
    THU & 0.175 & 0.150 & \textbf{0.147} & 0.187 & 0.185 & \textbf{0.151} \\ \hline
    FRI & 0.213 & 0.206 & \textbf{0.154} & 0.180 & 0.193 & \textbf{0.137} \\ \hline
    SAT & \textbf{0.205} & 0.231 & 0.207 & 0.238 & 0.246 & \textbf{0.237} \\ \hline
    SUN & 0.208 & \textbf{0.152} & 0.157 & 0.224 & 0.225 & \textbf{0.172} \\ \hline
  \end{tabular}
\end{table}

我们首先给出了在一周观测数据（2012.08.19$\sim$2012.08.25）的基础上，所获得的群体
时空分布模型的预测结果，以及区域和时间因素对预测性能的影响。对于每种模型，我们采
用期望最大算法对模型参数进行估计，而较大的时间和空间跨度带来了参数估计过程中的计
算开销。依据\ref{sec:gstpat:multiscale}节中的观测，由于高峰时段人们移动的特征距
离约为5公里，因此我们将模型可见的最大空间跨度为$h_{max}=5km$，同理，由于人们移动
行为的时间相关性的震荡周期约为12小时，因此我们选择半周期作为模型可见的最大时间跨
度，即$u_{max}=6h$。这里模型可见指在给定的时间和空间跨度范围内，模型能够利用历史
的信息对未来进行准确的预测；否则，由于模型未包含范围之外的时空关联信息而无法得到
有效预测。在模型训练阶段，我们利用过去24小时的历史数据对模型进行学习，然后利用得
到的模型对未来6小时的群体分布进行预测。由于试验中模型需要冷启动，因此我们最开始
的训练阶段基于周一的观测数据完成；接着重复上述步骤，直到余下的天数都被完整预测。
同时，我们选取了观测城市中不同类型的区域，其中商业区范围为$p_1 =
[120.167722,30.255391]$和$p_2=[120.197546,30.281592]$确定的矩形区域，住宅区由$p_1
= [120.16678,30.300872]$和$p_2
= [120.200125,30.336536]$确定，且两者的边长距离约为$4km
\sim 5km$。在空间上，我们对每个区域分割成$10
\times 10$的网格，平均每个网格的面积与单个基站的覆盖范围相当。从而我们得到
\ref{eq:rmse}式中的时间和空间索引分别为$N=100$和$T=144$。

表\ref{tbl:rmsestat}展示了利用上述实验环境的RMSE数据。可以看出，在住宅区域，融合
了时空关联信息的行为模型（即ST-Model）在星期三、四、五表现出较好的性能，且比无时
空关联的模型（即ExpModel和CauchyModel）性能提高了约2.8\%$\sim$25.2\%；但是在周二
和周末三天，后者表现出稍微的性能优势。在商业区域内，融合了时空关联信息的模型在大
多数观测时间内都取得了较好的预测性能，且预测准确度提高了约3.7\%$\sim$23.6\%。由
此可见，时间和空间的关联信息在预测群体时空分布行为上起着重要的作用，从而也表明研
究群体行为的时空依赖关系具有重要的价值。

\begin{figure}[!tb]
  \centering
  \subfigure[Rural Area]{\label{fig:predcmprural}
    \includegraphics[width=0.42\textwidth]{chap4/stcov_cmp_gneiting_rural.eps}}
  \subfigure[Urban Area]{\label{fig:predcmpurban}
    \includegraphics[width=0.42\textwidth]{chap4/stcov_cmp_gneiting_urban.eps}}
  \bicaption[fig:predcmpreal]{基于时空关联的ST-Model人群预测值与观测值的对比}{基
    于时空关联的ST-Model人群预测值与观测值的对比}{Fig}{Comparing population
    prediction with empirical observations (e.g., Friday).}
\end{figure}

为了解释融合时空关联信息的行为模型的细节特征，我们在图\ref{fig:predcmpreal}中展
示了在周四的24小时内，模型的预测值和实际观测值的散点分布（注：为了对比的需要，我
们将不同区域的观测量值进行了正规化）。在这组散点图中，越接近于对角线的点表示预测
得越准确，其中红色直线表示数据点的线性回归线。这里我们可以得出两个显著的结论：1）
融合了时空依赖信息的行为模型倾向于产生比较平滑的预测结果，这意味着，和实际观测相
比，低值范围倾向于获得较大的预测结果，而高值范围则较容易获得较小的预测值。这个特
点来自于行为模型本身，如\ref{eq:prediction}式所示，由于预测结果由历史时空点的线
性组合而成，因此和一维的线性回归类似，表现出一定的平滑作用；且时空相关性越强，平
滑效果越明显。2）预测区域的功能差异对模型的预测性能具有一定的影响，如商业区较住
宅区的RMSE数据小，即0.137 vs. 0.154。这是由于不同功能的区域，移动网络基站根据该
区域人们的生活习惯部署方式也不同，进而表现在不同观测点的人群分布差异较大；而这样
的差异与结论1中的模型预测机制相互作用，便出现结果的不同。例如，
在\ref{fig:predcmpreal}b中，由于商业区域的模型在$0.5 \sim 0.75$范围内预测性能较
好（即预测和观测值接近），而该区域的多数观测点处于该范围内，因此其RMSE数值比住宅
区域小。

\begin{sidewaysfigure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{chap4/stcov_thu_perf.eps}
  \bicaption[fig:modelchars]{不同模型在考虑区域差异下的协方差分布}{不同模型在考
    虑区域（时段为THU）差异下的协方差分布}{Fig}{The difference in
    spatio-temporal correlation for different models.}
\end{sidewaysfigure}

另一方面，ST-Model的性能优势体现在对时空关联特征有效利用的基础上，而模型的机制不
同，模型训练后所得的时空相关性特点也因此不同。这里我们展示
了ExpModel、CauchyModel和ST-Model在考虑区域差异下（时段为THU），不同模型所捕获的
时空关联性的差异。如图\ref{fig:modelchars}所示，在每对特征组合下（
如ExpModel-Rural），三个子图分别表示时空协方差函数$C(h, u)$，纯时间协方差分
布$C(0,u)$，以及纯空间协方差分布$C(h,
0)$。一般而言，曲线（面）的曲率越大，所在时间或空间跨度的相关程度越强。可以看出，
在住宅区域中，ExpModel的时间和空间跨度特征值（即曲率最大的点）分别为$u=2h$和
$h=1km$；CauchyModel的特征值分别为$u=1h$和$h=0.7km$；而ST-Model的特征值介于两者
之间，且倾向于ExpModel的特征，即$u=1.5h$和$h=0.9km$。在大于特征值的区域，时空相
关性逐步递减，且递减速度$v(\mbox{CauchyModel})>
v(\mbox{ST-Model}) > v(\mbox{ExpModel})$。然而，在商业区中，时空相关性表现出与
住宅区不同的特征，其中ExpModel的时间特征曲率较均匀，因此具有较大的时间特征值
$u=6h$，且空间特征值为$h=0.8km$；CauchyMode的时间和空间特征值分别为$u=2h$和
$h=0.7km$；而该区域中ST-Model与前两者的差异较大，其时间特征值在三类模型中最小，
即$u=1h$，且空间特征值为$h=0.8km$。在大于特征值的区域，时空相关性的递减趋势也表
现出与住宅区的差异，即$v(\mbox{ST-Model})>
v(\mbox{CauchyModel}) > v(\mbox{ExpModel})$。和表\ref{tbl:rmsestat}进行对比分析
可以发现，在住宅区域，由于CauchyModel和ST-Model的时空相关性趋势较为接近，因此其
预测性能也相当，分别为$\mbox{CM}_{rmse}=0.150$和$\mbox{STM}_{rmse}=0.147$；同理，
在商业区中，由于ExpModel和CauchyModel均与ST-Model的时空相关性趋势差异较大，因此前两者的预测
性能也远低于融合了时空相关性的行为模型。

综上所述，利用时空统计模型对群体行为的分布特征进行研究，其准确度主要体现在两个方
面：一是时间和空间的协方差函数能够捕获实际观测数据的特征，二是时空相关性信息能够
被模型有效地利用。虽然ExpModel和CauchyModel利用单纯的时间或空间相关性具有一定的
预测能力，且在某些取值范围内具有较好的性能优势，但是由于ST-Model在以上两个条件都
得到了满足，因此总体上性能优势明显。

\section{本章小结}


本章基于第\ref{chap:istpat}章中介观模式所揭示的个体移动的时空依赖性，进而对群体
移动的时空分布特点进行实证及建模研究。与传统的时间序列分析不同，本研究在把握不同
尺度（校园、城市、国家）下人群分布特点的基础上，找出时间和空间的交互关系，进而对
时空相关性进行统一建模。理论上来讲，这样的建模方法符合人群移动的内在规律，即同时
包括空间上的网络效应和时间上的时律性。利用多空间尺度的观测数据，我们对探究人群时
空分布的特点提供了一个新的视角。从空间角度来看，空间异质性在较大空间尺度（如城市
和国家）下普遍存在，从而突破了以往研究和模型中对人群均匀分布的假设，例如，可以帮
助移动网络网络设计人员避免网络资源的浪费，以及降低网络级联故障的发生概率。从时间
角度来看，了解人群在基站和区域范围内的动态变化，也有助于研究人员开发考虑时间相关
效应的预判模块（Proactive modules），从而提前对网络拥塞进行调节和控制。

同时，本研究的一些方法和结论可以在未来的工作中得到进一步发展和探索。其中一项有趣
的扩展工作是将我们研究的时空统计特征和个体移动行为的语义特征（如交通方式、出行目
的等）进行关联分析，从而将人群分布的空间异质性、时空依赖性与个体移动的时空模式进
行融合，进而将微观和宏观观测统一起来。另一项有趣的扩展是，本研究的时空统计模型虽
然能够捕捉不同区域和时间段内的差异，但是一个潜在的挑战在于开发出高效的自动化算法，
帮助检测出符合相同分布模式的连续时间和空间范围。这里提到的任何一项扩展都有助于我
们更加深刻地理解人群在时空上的分布特征，进而开发出更加准确、实用的群体移动模型。
